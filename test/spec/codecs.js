'use strict';

describe('Fragments: Codecs', function () {

    it('xpath codec', function () {

        var xpathFrag = {
            startXpath: "start",
            startOffset: '0',
            endXpath: "end",
            endOffset: '0'
        }

        var codec = FRAGVIZ.CODECS.findCodec("xpath")

        var encoded = codec.encode(xpathFrag)
        expect(encoded).toEqual("start,0,end,0")
        expect(codec.decode(encoded)).toEqual(xpathFrag)


    });


});
