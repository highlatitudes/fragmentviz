'use strict';

describe('Fragments: Resources', function () {

    it('Resource from URI', function () {

        var res = FRAGVIZ.RESOURCE.FROMURI("http://video-js.zencoder.com/oceans-clip.mp4#t=0,10")

        expect(res.srcBase).toEqual("http://video-js.zencoder.com/oceans-clip.mp4")
        expect(res.srcFrag).toEqual(
            { query : {  },
              hash : {
                  t : [ { value : '0,10', unit : 'npt', start : '0', end : '10', startNormalized : 0, endNormalized : 10 } ]
              }
            })
        expect(res.getSrcMimeType()).toEqual('video/mp4')

    });


});
