'use strict';

describe('Fragments: Viewers', function () {

    it('Find viewer', function () {

        var resUrl = "http://video-js.zencoder.com/oceans-clip.mp4#t=0,10"
        var viewer = FRAGVIZ.VIEWERS.findMatchingViewer(resUrl)

        expect(viewer.viewerName).toEqual("VideoJS Viewer")

    });


});
