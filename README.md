Fragments Viz Library
=====================

## Installation & Usage
Install using Bower :
``` 
$ bower install fragmentsviz -S
```


## Test/demo webapp
Start app using grunt :
```
$ grunt serve --watch
# Serve site locally. Now browse to http://localhost:8080 for examples
```

## Author
Philippe Duchesne [@pduchesne](https://twitter.com/pduchesne).

## Credits
Thomas Steiner ([@tomayac](https://twitter.com/tomayac)) for the [MediaFragments](http://tomayac.com/mediafragments/mediafragments.html) lib 
Steven Levithan ([stevenlevithan.com](stevenlevithan.com)) for the parseURI function
