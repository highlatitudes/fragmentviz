// Karma configuration
// http://karma-runner.github.io/0.10/config/configuration-file.html

module.exports = function(config) {
  config.set({
    // base path, that will be used to resolve files and exclude
    basePath: '',

    // testing framework to use (jasmine/mocha/qunit/...)
    frameworks: ['jasmine'],

    reporters: ['progress'],

    // list of files / patterns to load in the browser
    files: [
      'app/scripts/libs/underscore/underscore.js',
      'app/scripts/libs/d3/d3.js',
      'app/scripts/libs/jquery/jquery.js',
      'app/scripts/libs/jquery-ui/jquery-ui.js',
      'app/scripts/libs/MediaFragmentsURI/mediafragments.js',
      'app/scripts/libs/videojs/dist/video-js/video.js',
      'app/scripts/libs/openlayers2/OpenLayers.debug.js',
      'app/scripts/libs/ol-helpers/ol-helpers.js',
      'app/scripts/libs/ol-helpers/EsriGeoJSON.js',
      'app/scripts/d3graphs.js',
      'app/scripts/frag-codecs.js',
      'app/scripts/utils.js',
      'app/scripts/viewers.js',
      //'app/scripts/**/*.js',
      'test/mock/**/*.js',
      'test/spec/**/*.js' ,
      { pattern: 'app/views/**/*.html', included: false, served: true } // angular views ready to be served
    ],

    // list of files / patterns to exclude
    exclude: [],

    // web server port
    //port: 8080,     // defaults to 9876

    // level of logging
    // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // Start these browsers, currently available:
    // - Chrome
    // - ChromeCanary
    // - Firefox
    // - Opera
    // - Safari (only Mac)
    // - PhantomJS
    // - IE (only Windows)
    browsers: ['Chrome'],


    // Continuous Integration mode
    // if true, it capture browsers, run tests and exit
    singleRun: false
  });
};
