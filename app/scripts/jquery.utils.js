
(function($, undefined){

    var sumTop = function(elem, container) {
        var $inner = $(container)
        var top = 0;
        while (elem != $inner.offsetParent()[0]) {
            top += elem.offsetTop;
            elem = $(elem).offsetParent()[0];
        }

        return top;
    }

    $.widget('hilats.scrolllinks', {
        
        options: {

            anchorSelector: '.anchor',

            content: function (i, $elem) {
                return '<span class="scrolllink">' + (i + 1) + '</span>';
            },

            click: function(event, widget) {
                var $anchor = $($(event.target).data('anchor'))
                var anchorOffset = sumTop($anchor, widget.$inner)
                widget.$outer.animate({
                    scrollTop: $anchor.offset().top - widget.$outer.offset().top + widget.$outer.scrollTop() - 30
                });
            }
            
        },

        _create: function () {
            
            var self = this,
                $el = self.element;

            // -- taken from sausage.js --
            // Use $el for the outer element.
            self.$outer = $el;
            // Use `body` for the inner element if the outer element is `window`. Otherwise, use the first child of `$el`.
            // if outer is an iframe, take the contentDocument as inner
            if ($el.get(0).tagName == "IFRAME")
                self.$inner = $('body', $el.get(0).contentDocument);
            else if ($.isWindow($el.get(0)))
                self.$inner = $('body');
            else
                self.$inner = $el.children(':first-child');


            if (!self.$scrolllinks) {
                self.$scrolllinks = $("<div class='scrolllinks'></div>")
                self.$outer.append(
                    $("<div style='position: absolute; height: 100%; top: 0; right: 15px;'></div>").append(self.$scrolllinks)
                )
            }

            // Trigger the `create` event.
            self._trigger('create');

            return;
        },
        
        // ## `._init()`
        //
        //
        _init: function () {
            
            var self = this;

            
            self.draw();

            // Trigger the `init` event.
            self._trigger('init');
            
            return;
        },



        // # Public Methods
        //
        //

        draw: function () {

            var self = this;
            var contentHeight = self.$inner.height()

            self.$outer.find(self.options.anchorSelector).each(function(idx, elem) {
                var y = sumTop(elem, self.$inner)
                var link = $("<div class='scrolllink' style='top: "+(y*100/contentHeight)+"%'></div>")
                link.data('anchor', elem)

                self.$scrolllinks.append(link)

                if (self.options.click)
                    link.click(function(event) {
                        self.options.click(event, self)
                    })
                if (self.options.hover)
                    link.hover(function(event) {
                        self.options.hover(event, self)
                    })
            })

        },


        destroy: function () {
            
            var self = this;

            
            return;
        }
        
    });
    
})(jQuery);