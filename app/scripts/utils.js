/* *************************************
 HighLatitudes utils
 ************************************* */


(
    function () {
        var root = this;

        if (typeof root.FRAGVIZ === "undefined") root.FRAGVIZ = {}

        var UTILS = root.FRAGVIZ.UTILS = {

            proxyUrl: '/proxy',

            proxifyUrl: function(url, force) {
                var parsedUrl = UTILS.URI.parseUri(url)
                if (!force) {
                    force = parsedUrl.host != "" && parsedUrl.host != window.location.hostname;
                }

                if (force) {
                    var path = url
                    if (parsedUrl.query)
                        path = parsedUrl.protocol + '://' + parsedUrl.authority + parsedUrl.path
                    return UTILS.proxyUrl+'?_uri=' + encodeURIComponent(path) + (parsedUrl.query? ('&' + parsedUrl.query) : '')
                }
                else
                    return url
            },

            loadIntoFrame:function (url, frame, useProxy, callback) {

                //var urlNoQuery = url.substring(0,url.indexOf("?"))
                var proxiedUrl = this.proxifyUrl(url) // proxify if needed

                // force init with about page
                frame.src = 'about:'

                var timer
                function whenReady(frame, fn) {
                    var doc = frame.contentDocument;

                    if (doc.URL.indexOf("about:") !== 0) {
                        if (doc.readyState === "complete") {
                            console.warn('Frame already loaded')
                        } else {
                            if (timer) clearTimeout(timer);
                            fn(doc)
                        }
                    } else {
                        timer = setTimeout(function() {whenReady(frame, fn)}, 1);
                    }
                }


                if (frame.contentDocument) {
                    // try not to reload document
                    //TODO fails so far
                    frame.contentDocument.location = proxiedUrl
                } else {
                    frame.src = proxiedUrl
                }

                whenReady(frame, function(doc) {
                    // create an observer instance
                    var observer = new MutationObserver(function(mutations) {
                        mutations.forEach(function(mutation) {
                            if (mutation.addedNodes)
                                for (var idx = 0, length = mutation.addedNodes.length; idx < length; idx++) {
                                    if (mutation.addedNodes[idx].nodeName == 'HEAD') {
                                        var head = mutation.addedNodes[idx]
                                        head.appendChild(doc.createElement("base"))
                                        head.getElementsByTagName("base")[0].href = url;
                                    }
                                }
                        });
                    });

                    // configuration of the observer:
                    var config = { childList: true, subtree: true };

                    // pass in the target node, as well as the observer options
                    observer.observe(doc, config);
                })

                $(frame).load(function() {
                    // this causes the iframe to expand to its full height and width
                    //$(this)[0].style.height = $(this)[0].contentDocument.body.scrollHeight;
                    //$(this)[0].style.width = $(this)[0].contentDocument.body.scrollWidth;
                    if (callback) callback(this);
                });

                /*
                 FRAGVIZ.utils.callHTTP(, "GET", null, function(req) {
                 frame.contentDocument.innerHTML = req.responseXML | req.responseText;
                 })
                 */
            },

            loadScript: function(scriptUrl, callback, doc) {
                var d=doc || document,
                    s=d.createElement('script');
                s.src = scriptUrl;
                s.onload = function() {
                    if (callback) callback();
                };

                d.body.appendChild(s);
            },

            getContainingWindow: function(elem, defaultValue) {
                defaultValue = defaultValue || window;

                if (!elem) return defaultValue;
                if (elem.contentWindow) return elem.contentWindow; // this is an iframe
                else if (elem.ownerDocument) elem = elem.ownerDocument;

                if (elem.defaultView) return elem.defaultView;
                else if (elem.document) return elem; // this is already a window
                else return undefined;
            },

            computeTotalOffset: function(htmlElem) {

                var originalNode = htmlElem;
                var parentNode = htmlElem.parentNode;
                var tempNode;
                if (!htmlElem.offsetTop && !htmlElem.offsetParent && parentNode) {
                    if (htmlElem.nodeName == "#text") {
                        originalNode = htmlElem
                        tempNode = $("<span></span>")[0]
                        tempNode.innerText = originalNode.textContent
                        parentNode.replaceChild(tempNode, originalNode)
                        htmlElem = tempNode;
                    } else { // ???
                        htmlElem = parentNode;
                    }
                }


                var x = 0, y = 0, height = htmlElem.offsetHeight, width= htmlElem.offsetWidth;
                if (htmlElem.offsetParent && htmlElem.parentNode && (htmlElem.offsetLeft + width > htmlElem.parentNode.offsetWidth)) {
                    x = htmlElem.parentNode.offsetLeft - htmlElem.offsetLeft
                }


                do {
                    y += htmlElem.offsetTop || 0;
                    x += htmlElem.offsetLeft || 0;
                } while (htmlElem = htmlElem.offsetParent);

                if (tempNode) parentNode.replaceChild(originalNode, tempNode)

                return [x, y, width, height];
            },

            computePreciseRangeXywhOld: function(originalRange) {

                if (!originalRange) return;

                if (originalRange.startContainer == originalRange.endContainer) {
                    var range = originalRange.cloneRange();

                    var $span = $("<span></span>")

                    var minx, miny, maxx, maxy;

                    while (!range.collapsed && range.startOffset < range.startContainer.length-1) {
                        range.insertNode($span[0]);
                        //TODO improve : do not use computeTotalOffset which traverses the whole tree each time
                        var spanOffset = this.computeTotalOffset($span[0]);
                        minx = minx ? Math.min(minx, spanOffset[0]) : spanOffset[0];
                        miny = miny ? Math.min(miny, spanOffset[1]) : spanOffset[1];
                        maxx = maxx ? Math.max(maxx, spanOffset[0]+spanOffset[2]) : spanOffset[0]+spanOffset[2];
                        maxy = maxy ? Math.max(maxy, spanOffset[1]+spanOffset[3]) : spanOffset[1]+spanOffset[3];

                        $span.remove()
                        //TODO: improve : allow single-char text nodes, then normalize at the end
                        range.startContainer.parentNode.normalize()
                        range.setStart(range.startContainer, range.startOffset+1);
                    }

                    return [minx, miny, maxx, maxy];
                } else {
                    var xywhStartOffset = FRAGVIZ.UTILS.computeTotalOffset(originalRange.startContainer);
                    var xywhEndOffset = FRAGVIZ.UTILS.computeTotalOffset(originalRange.endContainer);
                    var startX = Math.min(xywhStartOffset[0],xywhEndOffset[0]);
                    var startY = Math.min(xywhStartOffset[1],xywhEndOffset[1]);
                    var endX = Math.max(xywhStartOffset[0]+xywhStartOffset[2],xywhEndOffset[0]+xywhEndOffset[2]);
                    var endY = Math.max(xywhStartOffset[1]+xywhStartOffset[3],xywhEndOffset[1]+xywhEndOffset[3]);

                    return [startX, startY, endX, endY];
                }

            },

            computePreciseRangeXywh: function(originalRange) {

                if (!originalRange) return;

                var range = originalRange.cloneRange();

                var $span = $("<span></span>")

                var minx, miny, maxx, maxy;

                var curContainer = range.startContainer;

                while (!range.collapsed && curContainer) {

                    var offsetBox;
                    var ended = false;
                    if (curContainer == originalRange.startContainer && curContainer == originalRange.endContainer) {
                        var tempRange = originalRange.cloneRange()
                        tempRange.surroundContents($span[0])

                        var spanOffset = this.computeTotalOffset($span[0]);
                        offsetBox = [spanOffset[0], spanOffset[1], spanOffset[0]+spanOffset[2], spanOffset[1]+spanOffset[3]]

                        var parentNode = $span[0].parentNode
                        var textNode = parentNode.ownerDocument.createTextNode($span[0].innerText)
                        parentNode.replaceChild(textNode, $span[0])
                        parentNode.normalize()
                        ended = true
                    }
                    else if (curContainer == originalRange.startContainer) {
                        var tempRange = originalRange.cloneRange()
                        tempRange.setEnd(tempRange.startContainer, tempRange.startContainer.length-1)
                        tempRange.surroundContents($span[0])

                        var spanOffset = this.computeTotalOffset($span[0]);
                        offsetBox = [spanOffset[0], spanOffset[1], spanOffset[0]+spanOffset[2], spanOffset[1]+spanOffset[3]]

                        var parentNode = $span[0].parentNode
                        var textNode = parentNode.ownerDocument.createTextNode($span[0].innerText)
                        parentNode.replaceChild(textNode, $span[0])
                        parentNode.normalize()
                    } else if (curContainer == originalRange.endContainer) {
                        var tempRange = originalRange.cloneRange()
                        tempRange.setStart(tempRange.endContainer, 0)
                        tempRange.surroundContents($span[0])

                        var spanOffset = this.computeTotalOffset($span[0]);
                        offsetBox = [spanOffset[0], spanOffset[1], spanOffset[0]+spanOffset[2], spanOffset[1]+spanOffset[3]]

                        var parentNode = $span[0].parentNode
                        var textNode = parentNode.ownerDocument.createTextNode($span[0].innerText)
                        parentNode.replaceChild(textNode, $span[0])
                        parentNode.normalize()
                        ended = true
                    } else {
                        // intermediary node
                        var xywhOffset = FRAGVIZ.UTILS.computeTotalOffset(curContainer)
                        offsetBox = [xywhOffset[0], xywhOffset[1], xywhOffset[0]+xywhOffset[2], xywhOffset[1]+xywhOffset[3]]
                    }

                    minx = minx ? Math.min(minx, offsetBox[0]) : offsetBox[0];
                    miny = miny ? Math.min(miny, offsetBox[1]) : offsetBox[1];
                    maxx = maxx ? Math.max(maxx, offsetBox[2]) : offsetBox[2];
                    maxy = maxy ? Math.max(maxy, offsetBox[3]) : offsetBox[3];

                    if (!ended) {
                        var nextContainer = curContainer;
                        while (nextContainer && !nextContainer.nextSibling) nextContainer = nextContainer.parentNode

                        if (!nextContainer) throw "Unexpected end of DOM tree, looking for end container"

                        curContainer = nextContainer.nextSibling

                        while (curContainer != originalRange.endContainer && $(curContainer).has(range.endContainer).length > 0 && curContainer.hasChildNodes())
                            curContainer = curContainer.childNodes.item(0)
                    } else {
                        curContainer = undefined;
                    }
                }

                return [minx, miny, maxx, maxy];

            },


            parents: function(node) {
                var nodes = [node]
                for (; node; node = node.parentNode) {
                    nodes.unshift(node)
                }
                return nodes
            },

            commonAncestor: function(node1, node2) {
                var parents1 = this.parents(node1)
                var parents2 = this.parents(node2)

                if (parents1[0] != parents2[0]) throw "No common ancestor!"

                for (var i = 0; i < parents1.length; i++) {
                    if (parents1[i] != parents2[i]) return parents1[i - 1]
                }
            },

            getSelection:function (elem) {

                var win = UTILS.getContainingWindow(elem);
                var doc = win.document || document;

                var userSelection;
                if (win.getSelection) {
                    userSelection = win.getSelection();
                }
                else if (doc.selection) { // should come last; Opera!
                    userSelection = doc.selection.createRange();
                }

                return userSelection;

            },

            getSelectionRange:function (iframe) {

                var userSelection = this.getSelection(iframe);

                if (!userSelection || userSelection.rangeCount == 0) return undefined;

                if (userSelection.getRangeAt)
                    return userSelection.getRangeAt(0);
                else { // Safari!
                    var range = document.createRange();
                    range.setStart(userSelection.anchorNode, userSelection.anchorOffset);
                    range.setEnd(userSelection.focusNode, userSelection.focusOffset);
                    return range;
                }

            },

            setSelectionRange:function (iframe, range) {

                var userSelection = this.getSelection(iframe);
                if (!userSelection) return;

                //TODO support opera --> use rangy

                userSelection.removeAllRanges();
                userSelection.addRange(range);

            },

            createSelectionRange:function (startElem, startOffset, endElem, endOffset, doc) {
                doc = doc || document;

                var range = doc.createRange();

                range.setStart(startElem, startOffset);
                range.setEnd(endElem, endOffset);

                return range;
            },

            /**
             * Creates a selection range from a parsed XPath fragment
             */
            getRangeFromXPathFragment:function (startXPath, startOffset, endXPath, endOffset, ctxDoc) {
                // add /text() where needed - overkill, should be done on save
                if ((startOffset || startOffset == 0) && startXPath.indexOf("text()")<0) startXPath += "/text()";
                if ((endOffset || endOffset == 0) && endXPath.indexOf("text()")<0) endXPath += "/text()";

                try {
                return this.createSelectionRange(
                    this.resolveXPath(startXPath, ctxDoc),
                    startOffset,
                    this.resolveXPath(endXPath, ctxDoc),
                    endOffset,
                    ctxDoc
                );
                } catch (err) {
                    FRAGVIZ.LOG.warn(undefined, err)
                    throw "Failed to create selection range from \n"+
                          startXPath+" : "+startOffset +"\n" +
                          endXPath+" : "+endOffset +"\n"
                }

            },


            /**
             * Resolves an xpath on the given document and returns the first matching node
             * @param xpath
             * @param ctxDoc
             */
            resolveXPath: function(xpath, ctxDoc) {
                return ctxDoc.evaluate(xpath,ctxDoc, null, XPathResult.ANY_TYPE, null).iterateNext() ;
            },

            getXPath:function (currentNode) {
                var path = [];
                // Walk up the Dom
                while (currentNode != undefined && currentNode.nodeType != 9 /* root document*/ ) {
                    var pe = this.getNode(currentNode);
                    if (pe != null) {
                        path.push(pe);
                        if (pe.indexOf('@id') != -1)
                            break;  // Found an ID, no need to go upper, absolute path is OK
                    }
                    currentNode = currentNode.parentNode;
                }
                return "/" + path.reverse().join('/');

            },

            getNode:function (node) {
                var nodeExpr = node.tagName;
                if (nodeExpr == null)  // Eg. node = #text
                    nodeExpr = "text()"
                    //return null;
                if (node.id && node.id != '') {
                    nodeExpr += "[@id='" + node.id + "']";
                    // We don't really need to go back up to //HTML, since IDs are supposed
                    // to be unique, so they are a good starting point.
                    return "/" + nodeExpr;
                }
// We don't really need this
//~   if (node.className != '')
//~   {
//~     nodeExpr += "[@class='" + node.className + "']";
//~   }
                // Find rank of node among its type in the parent
                var rank = 1;
                var ps = node.previousSibling;
                while (ps != null) {
                    if (ps.tagName == node.tagName) {
                        rank++;
                    }
                    ps = ps.previousSibling;
                }
                if (rank > 1) {
                    nodeExpr += '[' + rank + ']';
                }
                else {
                    // First node of its kind at this level. Are there any others?
                    var ns = node.nextSibling;
                    while (ns != null) {
                        if (ns.tagName == node.tagName) {
                            // Yes, mark it as being the first one
                            nodeExpr += '[1]';
                            break;
                        }
                        ns = ns.nextSibling;
                    }
                }
                return nodeExpr;
            },

            /**
             * Parses an URI fragment.
             * Resulting structure is
             *
             * { hash: {
             *    t: [ {
             *      value: ,
             *      unit: [npt|'prefix'|clock|pixel|percent],
             *      start: ,
             *      end: ,
             *      startNormalized: ,
             *      endNormalized:
             *    }* ],
             *
             *    xywh: [ {
             *      value: ,
             *      unit: [pixel|percent],
             *      x: ,
             *      y: ,
             *      w: ,
             *      h:
             *    }* ],
             *
             *    name: ,
             *    chapter: ,
             *    id: ,
             * }
             *
             * @param fragment
             */
            parseFragment: function(fragment) {
                //if (typeof fragment === "string") fragment = UTILS.URI.parseKeyValues(fragment)
                //TODO merge CODECS and MediaFragments lib
                if (MediaFragments)
                    return MediaFragments.parse(fragment);
                else
                    alert(LOG.error("Missing library to parse fragments"));
            },

            encodeHash: function(hash) {
                hash = (hash && hash.hash) || hash; // if hash contains a 'hash' subobject, take it
                var encodedHash = "";

                _.each(hash, function(hashValues, hashKey) {
                    var encoder = FRAGVIZ.CODECS.findCodec(hashKey);
                    if (encoder) _.each(hashValues,
                        function(hashValue) {
                            encodedHash += (encodedHash.length>0?'&':'')+encoder.hashKey+"="+encoder.encode(hashValue);
                        })

                })

                return encodedHash;
            },


            /**
             * Test func to add overlay on top of iframe - mimic iframefix from jquery
             */
            addIFrameOverlay: function() {
                $("iframe").each(function() {
                    $('<div class="ui-draggable-iframeFix" style="background: #fff;"></div>')
                        .css({
                            width: this.offsetWidth+"px", height: this.offsetHeight+"px",
                            position: "absolute", opacity: "0.001", zIndex: 1000
                        })
                        .css($(this).offset())
                        .appendTo("body");
                });
            },

            parseCsv:function (url, callback) {
                $.get("/app/proxy?url="+url, function (data) {
                    var dataSeries = [];

                    // Split the lines
                    var lines = data.split('\n');
                    $.each(lines, function (lineNo, line) {
                        var dataSerie = [];
                        var items = line.split(',');

                        $.each(items, function (itemNo, item) {
                            var val2;
                            try {
                                val2 = eval(item);
                            } catch(e) {
                                val2 = item;
                            }
                            var val = parseFloat(val2);
                            if (isNaN(val)) val = val2 || null; // put null instead of undefined, as highcharts supports null better
                            dataSerie.push(val);
                        });

                        if (dataSerie.length>0 && dataSerie[0] && dataSerie[0] != "") dataSeries.push(dataSerie);
                    });

                    if (callback) callback(dataSeries);
                });
            } ,

            /**
             *  Returns the content of the annotatin, oriented with the given URI as the source :
             *  {
             *     url :  ,
             *     fragment :  ,
             *     isTo : ,
             *     linkedResource : {
             *       url :
             *       fragment :
             *     }
             *  }
             *  If both to/from uris target the URI, 'from' is considered the source
             * @param annotation
             */
            getDirectedPair: function(annotation, rootUri) {
                var location = FRAGVIZ.UTILS.URI.parseUri(rootUri).unfragmented

                var result = undefined
                if (annotation.fromUri == location) {
                    result = {
                        url: location,
                        fragment : annotation.fragment,
                        linked: {
                            url: annotation.toUri,
                            fragment : annotation.toFragment
                        },
                        isTo: false
                    }
                }
                else if (annotation.toUri == location) {
                    result = {
                        url: location,
                        fragment : annotation.toFragment,
                        linked: {
                            url: annotation.fromUri,
                            fragment : annotation.fragment
                        },
                        isTo: true
                    }
                }
                else {}

                return result
            }

        };



        /** ********************
         *      Mime Types
         *  ******************** */

        UTILS.MIME = {
            // private fields for mime extension mappings
            mimeExtensions : {},
            // holds regex that match dimensions. e.g. {"video/*" : "video"}
            mimeDimensions : {},
            typeMap : {},

            DIMENSIONS : {
                TEXT: 'text',
                AUDIO: 'audio',
                VIDEO: 'video',
                SPATIAL: 'spatial',
                TABULAR: 'tabular'
            },

            /**
             * Register a set of mime extension, in the form
             * {
             *   ext: mimeType,
             *   ...
             * }
             * @param extensionMap
             */
            registerMimeExtensions : function(extensionMap) {
                for (var ext in extensionMap) {
                    this.registerMimeExtension(ext.toLowerCase(), extensionMap[ext].toLowerCase());
                }
            },

            /*
             trimPath: function(url) {
             if (!url) return
             url = url.toString();
             if (url.endsWith('/')) return url.substring(0, url.length-1);
             else return url;
             },
             */

            registerMimeExtension : function(extension, mimeType) {
                if (this.mimeExtensions[extension.toLowerCase()] && this.mimeExtensions[extension.toLowerCase()] != mimeType.toLowerCase())
                    throw "Extension '"+extension.toLowerCase()+"' already defined for " + this.mimeExtensions[extension.toLowerCase()]

                this.mimeExtensions[extension.toLowerCase()] = mimeType.toLowerCase();
                this.typeMap[mimeType.toLowerCase()] = extension.toLowerCase();
            },

            registerMimeDimensions : function(dimensionsMap) {
                var dimensions = this.mimeDimensions
                _.each(dimensionsMap, function(dim, mimeRegex) {
                    dimensions[mimeRegex] = dim;
                });
            },

            getMimeDimension: function(mimeType, uri) {
                //TODO centralize parsing of site-specific mime types (youtube, ...)
                mimeType = mimeType || FRAGVIZ.UTILS.MIME.getExtensionMimeType(UTILS.URI.parseUri(uri).extension);

                if (!mimeType) {
                    if (uri && FRAGVIZ.VIEWERS.YOUTUBE.extractYoutubeId(uri)) mimeType = "video/youtube";
                    else if (uri && FRAGVIZ.VIEWERS.VIMEO.extractVimeoId(uri)) mimeType = "video/vimeo";
                    else if (uri && FRAGVIZ.VIEWERS.SLIDESHARE.extractSlideshareId(uri)) mimeType = "application/slideshare";
                    else return FRAGVIZ.UTILS.MIME.DIMENSIONS.TEXT;
                }
                return _.find(this.mimeDimensions, function(dim, mimeRegex) {
                    return mimeType.match(mimeRegex);
                }) || FRAGVIZ.UTILS.MIME.DIMENSIONS.TEXT; // defaults to text
            },

            getMimeExtension: function(mimeType) {
                return this.typeMap[mimeType && mimeType.toLowerCase()];
            },

            // TODO replace existing usages by URI.parseUri where possible
            getExtensionMimeType: function(ext) {
                return this.mimeExtensions[ext && ext.toLowerCase()];
            }
        }

        UTILS.MIME.registerMimeExtensions({
            'txt':'text/plain',
            'htm':'text/html',
            'html':'text/html',
            'pdf': 'application/pdf',
            'webm':'video/webm',
            'webmv':'video/webm',
            'mp4': 'video/mp4',
            'm4v': 'video/mp4',
            'ogv': 'video/ogg',
            'mp3': 'audio/mp3',
            'm4a': 'audio/mp4',
            'oga': 'audio/ogg',
            'ogg': 'audio/ogg',
            'csv': 'application/csv',
            'kml': 'application/vnd.google-earth.kml+xml',
            'gml': 'application/gml+xml',
            'wms': 'application/vnd.ogc.wms_xml',
            'wfs': 'application/vnd.ogc.wfs_xml',
            'wmts': 'service/wmts',
            'arcgis_rest': 'application/vnd.esri.arcgis.rest',
            'geojson' : 'application/vnd.geo+json'
        });

        UTILS.MIME.registerMimeDimensions({
            'text/.*': UTILS.MIME.DIMENSIONS.TEXT,
            'application/slideshare': UTILS.MIME.DIMENSIONS.TEXT,
            'audio/.*': UTILS.MIME.DIMENSIONS.AUDIO,
            'video/.*': UTILS.MIME.DIMENSIONS.VIDEO,
            '.*/kml': UTILS.MIME.DIMENSIONS.SPATIAL,
            'application/gml+xml': UTILS.MIME.DIMENSIONS.SPATIAL,
            'application/vnd.ogc.wms_xml': UTILS.MIME.DIMENSIONS.SPATIAL,
            'application/vnd.ogc.wfs_xml': UTILS.MIME.DIMENSIONS.SPATIAL,
            'service/wmts': UTILS.MIME.DIMENSIONS.SPATIAL,
            'application/csv': UTILS.MIME.DIMENSIONS.TABULAR,
            'application/x-msdownload': UTILS.MIME.DIMENSIONS.TABULAR
        });


        /** ********************
         *      URI parsing
         *  ******************** */


        UTILS.URI = {
            // parseUri 1.2.2
            // (c) Steven Levithan <stevenlevithan.com>
            // MIT License

            // Modified to support file extension parsing, hash key values parsing, unfragmented part capture -- pduchesne, 2014-10-274
            parseUri :function(str) {
                var	o   = this.parseOptions,
                    m   = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
                    uri = {},
                    i   = this.parseOptions.key.length;

                while (i--) uri[o.key[i]] = m[i] || "";

                uri[o.qName] = this.parseKeyValues(uri[o.key[14]]);
                uri[o.hName] = this.parseKeyValues(uri[o.key[15]]);

                return uri;
            },

            parseKeyValues: function(str) {
                var val, valueMap = {}

                while (str && (val = this.parseOptions.keyValueParser.exec(str))) {
                    if (!valueMap[val[1]]) {
                        valueMap[val[1]] = val[2]
                    } else {
                        valueMap[val[1]] = Array.isArray(valueMap[val[1]]) ? valueMap[val[1]] : [valueMap[val[1]]]
                        valueMap[val[1]].push(val[2])
                    }
                }

                return valueMap
            },

            parseOptions : {
                strictMode: true,
                key: ["source","unfragmented","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","extension","query","hash"],
                qName :   "queryKey",
                hName :   "hashKey",
                keyValueParser: /(?:^|&)([^&=]*)=?([^&]*)/g,
                parser: {
                    // add extension parsing to the strict regex ; suppress the loose regex until it is modified identically
                    //strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
                    strict: /^((?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)((?:[^?#]*\.)?([^?#]*)))(?:\?([^#]*))?))(?:#(.*))?/
                    //loose:  /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
                }
            }
        }


        /** ********************
         *      Logging Tool
         *  ******************** */

        var LOG = root.FRAGVIZ.LOG = {

            DEBUG:1000,
            INFO:100,
            WARN:2,
            ERROR:1,

            logger:null,
            level:2,
            consoleLevel:2,


            initLog:function (el, maxLevel) {
                this.logger = el;
                if (maxLevel) this.level = maxLevel;
            },

            warn:function (msg, err) {
                this.output(msg, err, this.WARN);
            },
            info:function (msg, err) {
                this.output(msg, err, this.INFO);
            },
            error:function (msg, err) {
                this.output(msg, err, this.ERROR);
            },
            debug:function (msg, err) {
                this.output(msg, err, this.DEBUG);
            },

            output: function(msg, err, logLevel) {
                if (this.level >= logLevel && this.logger)
                    $(this.logger).append("<p class='log_" +logLevel+"'>"+msg+"<br/>"+err+"</p>")
                if ((this.level >= logLevel && !this.logger) || this.consoleLevel >= logLevel) {
                    var logMeth;
                    switch (logLevel) {
                        case this.ERROR:
                            logMeth = console.error;
                            break;
                        case this.WARN:
                            logMeth = console.warn;
                            break;
                        case this.INFO:
                            logMeth = console.info;
                            break;
                        case this.DEBUG:
                        default:
                            logMeth = console.debug;
                            break;
                    }
                    if (msg) logMeth.apply(console, [msg]);
                    if (err) {
                        logMeth.apply(console, [this.formatError(err)]);
                        if (typeof err == "object") logMeth.apply(console, [err]);
                    }
                }
            },

            formatError: function(err, separator) {
                if (typeof err == "string") return err

                var output = "";
                //for (var prop in err) output += "property: "+ prop+ " value: ["+ err[prop]+ "]"+(separator || "\n");
                // for chrome:
                output = err.message + (separator || '\n') + err.stack;
                return output;
            }
        }
    }).call(this);


if (!String.prototype.startsWith)
    String.prototype.startsWith = function(str) {
        return this.indexOf(str) == 0;
    }

String.prototype.endsWith = function(str) {
    //TODO crap. must be a better way.
    return this.length >= str.length && this.lastIndexOf(str) == this.length - str.length;
}


String.prototype.hashCode = function(){
    var hash = 0, i, char;
    if (this.length == 0) return hash;
    for (i = 0; i < this.length; i++) {
        char = this.charCodeAt(i);
        hash = ((hash<<5)-hash)+char;
        hash = hash & hash; // Convert to 32bit integer
    }
    return hash;
};

!function ($) {

    "use strict"; // jshint ;_;


    /* EDITABLE FIELD PLUGIN DEFINITION
     * ======================= */

     // TODO does bootstrap provide similar func?
    // add jQuery 'editable' function to make input fields editable
    $.fn.editable = function(callback, param) {

        var sDefaults = {
            size: 10
        }

        var options = $.extend(sDefaults, param);

        this.dblclick(function() {
            var editableElem = this;
            var curValue = $(this).text();
            var inputElem = $("<input>", {size: options.size, value: curValue})
                .keyup(function(e) {
                    if (e.keyCode == 13) {
                        $(editableElem).empty().append(this.value);
                        if (callback) callback(this.value, this);
                    }
                });
            $(this).empty().append(inputElem);
        })

        return this;
    }

}($);



