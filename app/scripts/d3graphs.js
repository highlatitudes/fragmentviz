
(function(){

    var root = this;
    var viewers = {};

    // fake the namespace definition
    if (typeof root.FRAGVIZ === "undefined") root.FRAGVIZ = {};

    var D3 = root.FRAGVIZ.D3 = {}


    D3.Graph = function(options) {
        this.options = options || {}

        this.options.margin = _.extend(
            {left: 30, top: 30, bottom: 30, right:60},
            this.options.margin || {})
    }
    D3.Graph.prototype = {
        initDisplay: function(node) {

            var svgWidth, svgHeight;
            var svgPixelWidth, svgPixelHeight;

            if (!this.options.width) {
                svgWidth = "100%"
                svgPixelWidth = node.offsetWidth
                this.options.width = svgPixelWidth - (this.options.margin.left + this.options.margin.right)
            } else {
                svgWidth = this.options.width
                svgPixelWidth = svgWidth + this.options.margin.left + this.options.margin.right
            }
            if (!this.options.height) {
                svgHeight = "100%"
                svgPixelHeight = node.offsetHeight
                this.options.height = svgPixelHeight - (this.options.margin.top + this.options.margin.bottom)
            } else {
                svgHeight = this.options.height
                svgPixelHeight = svgHeight + this.options.margin.top + this.options.margin.bottom
            }

            this.svg = d3.select(node)
                .append("svg")
            if (this.options.width) this.svg.attr("width", svgWidth)
            if (this.options.height) this.svg.attr("height", svgHeight)
            this.svg.attr("viewBox", '0 0 '+svgPixelWidth+' '+svgPixelHeight)

            this.g = this.svg.append("g")
                .attr("transform", "translate(" + this.options.margin.left + "," + this.options.margin.top + ")");

            $(node).resize(_.bind(this.refreshDisplay, this))
        },

        displayData: function(data) {
            throw "Not Implemented"
        },

        destroy: function() {
            if (this.svg) this.svg.remove()
        },

        refreshDisplay: function(e) {

        }

    }

    D3.XYGraph = function(options) {
        D3.Graph.apply(this, arguments)

        this.options = _.extend(
            { xProp: 0,
              yProp: 1},
            this.options)

    }
    _.extend (D3.XYGraph.prototype, D3.Graph.prototype, {
        initDisplay: function(node) {
            D3.Graph.prototype.initDisplay.apply(this, arguments)

            this.xScale = d3.scale.linear().range([0, this.options.width])
            this.yScale = d3.scale.linear().range([this.options.height, 0])
            this.xAxis = d3.svg.axis().scale(this.xScale).orient("bottom")
            this.yAxis = d3.svg.axis().scale(this.yScale).orient("left")

            /* Comment out to disable zoom */
            this.plot = this.g.append("rect")
                .attr("width", this.options.width)
                .attr("height", this.options.height)
                .style("fill", "#EEEEEE")
                .attr("pointer-events", "all")
            this.plot.call(d3.behavior.zoom().x(this.xScale).y(this.yScale).on("zoom", _.bind(this.zoomed, this)));

            this.g.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + this.options.height + ")")


            this.g.append("g")
                .attr("class", "y axis")
                .append("text")
                .attr("transform", "rotate(-90)")
                .attr("y", 6)
                .attr("dy", ".71em")
                .style("text-anchor", "end")
                .text("Y Title");

        },

        zoomed: function() {
            this.g.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
        },

        displayData: function(data) {
            throw "Not Implemented"
        }
    })


    D3.TemporalCurves = function(options) {
        D3.XYGraph.apply(this, arguments)
    }
    _.extend (D3.TemporalCurves.prototype, D3.XYGraph.prototype, {

        initDisplay: function(node) {
            D3.XYGraph.prototype.initDisplay.apply(this, arguments)

            var _thisGraph = this
            this.xAxis.scale(this.xScale = d3.time.scale().range([0, this.options.width]))
            this.line = d3.svg.line().interpolate("basis")
                .x(function(rowValues) {
                    return _thisGraph.xScale(rowValues.x)})
                .y(function(rowValues) {
                    return _thisGraph.yScale(rowValues.y)});

            this.color = d3.scale.category10()
        },

        parseOptionsDefaults: {
            timeCol: 'date',
            hasHeaders: true,
            headers: undefined,
            timeFormat: "%Y%m%d",
            transpose: false,
            skipRows: undefined
        },

        /**
         * Parses raw data to conform with the displayData input form
         *
         * Expected input:
         * [
         *   [ <header_i>,* ],?
         *   [ <value_i> ,* ],*
         * ]
         * where one of the columns is named after the 'timeCol' option and holds the date attribute
         *
         * Output:
         *
         * [{
         *     name: <header_i>
         *     values : [
         *      { x: <timestamp>,
         *        y: <value_i>
         *      }*
         *     ]
         * }*]
         *
         * @param rawData
         * @param options
         */
        parseData: function(rawData, options) {
            options = options || this.parseOptionsDefaults

            rawData = rawData.concat()
            if (options.transpose) rawData = d3.transpose(rawData)

            var names = options.headers || ( options.hasHeaders && rawData[0] ) || d3.range(rawData[0].length)

            if (options.skipRows) {
                var skipIdxes = options.skipRows.split(',').map(function(s) {return +s})
                rawData = rawData.filter(function(row, idx) {
                    return skipIdxes.indexOf(idx)<0 && (!options.hasHeaders || idx != 0)
                })
            }

            var timeIdx = (typeof options.timeCol == "number") ? options.timeCol : names.indexOf(options.timeCol)
            var serieNames = names.concat() // clone the array
            serieNames.splice(timeIdx, 1)   // remove the time column

            serieNames = options.valueCols || serieNames

            // parse dates across the raw array
            var parseDate = options.timeFormat == "iso" ?
                d3.time.format.iso.parse :
                d3.time.format(options.timeFormat).parse;
            rawData.forEach(function(row) {row[timeIdx] = parseDate(row[timeIdx]);});

            // compute the array of column indexes for the named series
            var namesIdx = serieNames.map(function(name) {return names.indexOf(name)} )

            var series = namesIdx.map(function(idx) {
                return {
                    name: names[idx],
                    values: rawData.map(function(rawRow) {
                        return {y: +rawRow[idx], x: rawRow[timeIdx]};
                    })
                };
            });

            return series;
        },

        /**
         * Data must be of the form:
         *
         * [{
         *     name: <serieName>
         *     values : [
         *      { x: <timestamp>,
         *       [y_i: <value>,]*
         *      }*
         *     ]
         * }*]
         *
         * @param data
         */
        displayData: function(series) {
            var _thisGraph = this
            this.currentData = series

            series.forEach(function(serie) {
                serie.values = serie.values.filter(function(row) {return row.x && row.y && row.x != '' && row.y != ''})
            })


            var names = series.map(function(e) {return e.name})
            this.color.domain(names);

            this.xScale.domain([
                d3.min(series, function(c) { return d3.min(c.values, function(row) { return row.x; }); }),
                d3.max(series, function(c) { return d3.max(c.values, function(row) { return row.x; }); })
            ]);

            this.yScale.domain([
                d3.min(series, function(c) {
                    return d3.min(c.values, function(row) {
                        return row.y; }); }),
                d3.max(series, function(c) {
                    return d3.max(c.values, function(row) {
                        return row.y; }); })
            ]);

            this.g.select(".x.axis").call(this.xAxis);
            this.g.select(".y.axis").call(this.yAxis);

            var serie = this.g.selectAll(".serie")
                .data(series)
                .enter().append("g")
                .attr("class", "serie");

            /*
            serie.append("path")
                .attr("class", "line")
                .attr("d", function(serie) { return _thisGraph.line(serie.values); })
                .style("stroke", function(serie) { return _thisGraph.color(serie.name); });
*/

            var x = function(d) {
                return _thisGraph.xScale(d.x);
            }
            var y = function(d) {
                return _thisGraph.yScale(d.y);
            }
            serie.selectAll(".bar")
                .data(function(serie) {
                    return serie.values
                })
                .enter().append("rect")
                .attr("class", "bar")
                .attr("x", x)
                .attr("width", function(d) {
                    return _thisGraph.xScale(d.x) - _thisGraph.xScale(new Date(d.x.getTime()-24*60*60*1000))
                })
                .attr("y", y)
                .attr("height", function(d) {
                    return _thisGraph.options.height - _thisGraph.yScale(d.y)
                })
                .append("svg:title")
                .text(function(d) { return d.x.toLocaleDateString()+" : "+d.y; });

            /* HAITI DEMO : use circles plot */
            /*
            serie.selectAll(".dot")
                .data(function(serie) {
                    return serie.values
                })
                .enter().append("circle")
                .attr("class", "dot")
                .attr("r", 3.5)
                .attr("cx", x)
                .attr("cy", y)
                .style("fill", function(d) {
                    return _thisGraph.color(this.parentNode.__data__.name);
                });
                */


            if (series.length == 1) this.g.select(".y.axis>text").text(series[0].name)
            else {
                serie.append("text")
                    .datum(function(serie) { return {name: serie.name, value: serie.values[serie.values.length - 1]}; })
                    .attr("transform", function(serie) {
                        return "translate(" + _thisGraph.xScale(serie.value.x) + "," + _thisGraph.yScale(serie.value.y) + ")";
                    })
                    .attr("x", 3)
                    .attr("dy", ".35em")
                    .text(function(serie) { return serie.name; });
            }
        },

        refreshDisplay: function() {

        }
    })


}) .call(this);