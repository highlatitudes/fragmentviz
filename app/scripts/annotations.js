/**
 * Created by IntelliJ IDEA.
 * User: pduchesne
 * Date: 19/02/12
 * Time: 15:48
 * To change this template use File | Settings | File Templates.
 */



(function(){

    var root = this;

    // fake the namespace definition
    if (typeof root.FRAGVIZ === "undefined") root.FRAGVIZ = {};

    // The top-level namespace. All public Backbone classes and modules will
    // be attached to this. Exported for both CommonJS and the browser.
    var ANNOT = root.FRAGVIZ.ANNOT = {};



    /** ***********************
     *     Model : Annotation
        *********************** */

    ANNOT.Annotation = function(fragment, dataObj) {

        $.extend(this,
            {
                fragment: fragment,   // the source fragment for this annotation
                href: dataObj.href || dataObj.target,            // if the annotation points to another resource (opt)
                html: dataObj.html || dataObj.description        // inline html/text for the annotation
            });

        // if no fragment provided, try to parse it from properties or src
        if (!this.fragment) {
            if (dataObj.fragment) this.fragment = FRAGVIZ.UTILS.parseFragment(dataObj.fragment);
            else {
                var srcBaseAndFrag = FRAGVIZ.UTILS.splitURIFragment(this.src);
                if (srcBaseAndFrag[1] && srcBaseAndFrag[1].length > 0) {
                    this.src = srcBaseAndFrag[0];
                    this.fragment = FRAGVIZ.UTILS.parseFragment('#'+srcBaseAndFrag[1]);
                }
            }
        }

        if (this.href) {
            var hrefBaseAndFrag = FRAGVIZ.UTILS.splitURIFragment(this.href);

            this.hrefSrc = hrefBaseAndFrag[0];
            if (hrefBaseAndFrag[1] && hrefBaseAndFrag[1].length > 0) this.hrefFrag = FRAGVIZ.UTILS.parseFragment('#'+hrefBaseAndFrag[1]);

        }
    };

    ANNOT.Annotation.prototype = {

        getSrcFragment: function() {return this.fragment},

        getSrcURI: function() {return this.src},

        getTargetFragment: function() {return this.hrefFrag},

        getTargetURI: function() {return this.hrefSrc},

        getId: function() {return this.resourceData.id},

        getDirections: function(location) {
            // watch out, annotations are trimmed by default when added (removing trailing slashes)
            location = HILATS.UTILS.trimPath(HILATS.UTILS.splitURIFragment(location.toString())[0])
            var result = {}
            if (this.getSrcURI() == location) {
                result.from = this.getSrcFragment()
            }
            if (this.getTargetURI() == location) {
                result.to =  this.getTargetFragment()
            }
            if (!result.from && !result.to) {
                return undefined
            }

            return result
        }
    };




    /** ***********************
     *     Model : Event
     *********************** */

    ANNOT.EVENTTYPES = {
        ADD: 0,
        REFRESH: 1
    }

    ANNOT.Event = function(type, list, annot) {
        this.type = type;
        this.list = list;
        this.annot = annot;
    };
    ANNOT.Event.prototype = {

    };




    /** ***************************
     *     Model : AnnotationList
     ****************************** */

    //REFACTOR replace with Link Collection
    ANNOT.AnnotationList = function(annotations) {
        _.extend(this, {
            annotations: [],
            listeners: []
        });
        if (annotations) this.annotations = annotations;
    };

    ANNOT.AnnotationList.prototype = {
        
        getTags: function() {
            //TODO
        },

        addListener: function(listenFunc) {
            this.listeners.push(listenFunc);
        },

        removeListener: function(listenFunc) {
            var idx = this.listeners.indexOf(listenFunc);
            if (idx >= 0) this.listeners.splice(idx,idx);
        },

        fireEvent: function(event) {
            _.each(
                this.listeners,
                function(listenFunc) {
                    listenFunc(event);
                },
                this
            )
        },

        addAnnotations:function (annotations) {
            _.each(
                annotations,
                function (elem, index) {
                    this.addAnnotation(elem);
                },
                this);
        },

        setAnnotations:function (annotations) {
            this.annotations = annotations;

            this.fireEvent(new ANNOT.Event(ANNOT.EVENTTYPES.REFRESH, this));
        },

        clear: function() {
            this.annotations = [];

            this.fireEvent(new ANNOT.Event(ANNOT.EVENTTYPES.REFRESH, this));
        },

        addAnnotation:function (annotation) {
            this.annotations.push(annotation);

            this.fireEvent(new ANNOT.Event(ANNOT.EVENTTYPES.ADD, this, annotation));
        },

        getById: function (id) {
            //TODO improve
            for (var idx in this.annotations) if (this.annotations[idx].getId() == id) return this.annotations[idx]
        }
        
    };

}) .call(this);