/* *************************************
 HighLatitudes utils
 ************************************* */

// TODO make this independant of underscore

(
    function () {
        var root = this;

        if (typeof root.FRAGVIZ === "undefined") root.FRAGVIZ = {};


        FRAGVIZ.CODECS = {

            registerInMediafragments: function () {
                // register new fragment parsers into MediaFragments
                if (typeof MediaFragments !== "undefined") {
                    MediaFragments.registerDimensionParser("xpath", FRAGVIZ.CODECS.xpathCodec.decode);
                    MediaFragments.registerDimensionParser("bbox", FRAGVIZ.CODECS.bboxCodec.decode);
                    MediaFragments.registerDimensionParser("page", FRAGVIZ.CODECS.pageCodec.decode);
                    //TODO
                    //MediaFragments.registerDimensionParser("col", FRAGVIZ.CODECS.csvFragCodec.decode);
                    //MediaFragments.registerDimensionParser("row", FRAGVIZ.CODECS.csvFragCodec.decode);
                    //MediaFragments.registerDimensionParser("cell", FRAGVIZ.CODECS.csvFragCodec.decode);

                }
            },

            findCodec: function(hashKey) {
                var codec;
                _.each(this, function(c) {if (c.hashKey == hashKey) codec = c})

                return codec;
            },

            xpathCodec: {

                hashKey: "xpath",

                /**
                 * Computes a selection range from xpath
                 * @param selectionRange
                 */
                getRangeFromXPathFragment:function (fragment, doc) {
                    if (!fragment) return undefined;

                    var startNode = FRAGVIZ.UTILS.resolveXPath(fragment.startXpath, doc)
                    var endNode = FRAGVIZ.UTILS.resolveXPath(fragment.endXpath, doc)

                    var range = doc.createRange();

                    range.setStart(startNode, fragment.startOffset);
                    range.setEnd(endNode, fragment.endOffset);

                    return range;
                },

                /**
                 * Return an HTML fragment as a set of XPaths :
                 *      "startXPath, startOffset, endXPath, endOffset"
                 * @param selectionRange
                 */
                getXPathFragmentFromRange:function (selectionRange) {
                    if (!selectionRange) return undefined;
                    return {
                        startXpath: FRAGVIZ.UTILS.getXPath(selectionRange.startContainer),
                        startOffset: selectionRange.startOffset,
                        endXpath: FRAGVIZ.UTILS.getXPath(selectionRange.endContainer),
                        endOffset: selectionRange.endOffset
                    }
                },

                encode: function(frag) {
                    if (!frag) return "";
                    return frag.startXpath + ',' +
                        frag.startOffset + ',' +
                        frag.endXpath + ',' +
                        frag.endOffset;
                },

                /**
                 * XPath parser.
                 * Accepts "[startXPath],[startOffset],[endXPath],[endOffset]"
                 * @param value the xpath parameter from the URI fragment
                 */
                decode: function(value) {
                    var components = value.split(',');
                    return {
                        startXpath: components[0],
                        startOffset: components[1],
                        endXpath: components[2],
                        endOffset: components[3]
                    };
                }
            },

            timeCodec: {
                hashKey: "t",

                /*
                 t: {
                 *      value: ,
                 *      unit: [npt|'prefix'|clock|pixel|percent],
                 *      start: ,
                 *      end: ,
                 *      startNormalized: ,
                 *      endNormalized:
                 *    }
                 */
                encode: function(timeFrag) {

                    //var result = t.start || 0
                    //result += t.end ? (','+ t.end) : ''

                    return timeFrag.value || (timeFrag.startNormalized + (timeFrag.endNormalized?(','+timeFrag.endNormalized):''));
                },

                decode: function(timeFrag) {
                    throw "not implemented"
                }
            },

            bboxCodec: {
                hashKey: "bbox",

                /*
                 bbox: {
                 *      value: [minx,miny,maxx,maxy]
                 *    }
                 */
                encode: function(bboxFrag) {

                    //var result = t.start || 0
                    //result += t.end ? (','+ t.end) : ''

                    return bboxFrag[0]+','+bboxFrag[1]+','+bboxFrag[2]+','+bboxFrag[3];
                },

                decode: function(bboxFrag) {
                    return _.map(bboxFrag.split(','), function(val) {return parseFloat(val)})
                }
            },

            rowCodec: {
                hashKey: "row",

                /*
                 row: {
                 *      value: [[i,j]*]
                 *    }
                 */
                encode: function(rowFrag) {
                    var encodedFrag = ""
                    _.each(rowFrag, function (rowRange) {
                        rowRange += (rowRange?';':'') + rowRange[0]+ (rowRange.length>0)?('-'+rowRange[1]):''
                    })
                    return encodedFrag
                },

                decode: function(rowFrag) {
                    return _.map(rowFrag.split(';'), function(rowRange) { _.map(rowRange.split('-'), function(rowId) {return parseInt(rowId)})})
                }
            },

            colCodec: {
                hashKey: "col",

                /*
                 col: {
                 *      value: [[i,j]*]
                 *    }
                 */
                encode: function(colFrag) {
                    var encodedFrag = ""
                    _.each(colFrag, function (colRange) {
                        colRange += (colRange?';':'') + colRange[0]+ (colRange.length>0)?('-'+colRange[1]):''
                    })
                    return encodedFrag
                },

                decode: function(colFrag) {
                    return _.map(colFrag.split(';'), function(colRange) { _.map(colRange.split('-'), function(colId) {return parseInt(colId)})})
                }
            },

            pixelSpaceCodec: {
                hashKey: "xywh",

                encode: function(timeFrag) {
                    // fragments can either be in the form [x,y,w,h] (parsed using mediagraments) or {x: , y: , w: , h:}
                    return (Array.isArray(timeFrag) ? timeFrag : [timeFrag.x, timeFrag.y, timeFrag.w, timeFrag.h]).join(",")
                },

                decode: function(timeFrag) {
                    throw "not implemented"
                }
            },

            pageCodec: {
                hashKey: "page",

                /*
                 col: {
                 *      value: i
                 *    }
                 */
                encode: function(pageFrag) {
                    return pageFrag
                },

                decode: function(pageFrag) {
                    return parseInt(pageFrag)
                }
            },

            //TODO is this needed? correct? redundant with id encoder in mediafragments
            idCodec: {
                hashKey: "id",

                /*
                 id: {
                 *      value: <id>
                 *    }
                 */
                encode: function(pageFrag) {
                    return pageFrag
                },

                decode: function(pageFrag) {
                    return pageFrag
                }
            }
        };

        FRAGVIZ.CODECS.registerInMediafragments()

    }).call(this);






