/**
 *
 * Defines the Viewer model and its various implementations (HTML, Text, Video, Youtube, Vimeo, ...)
 * Also contains a dispatcher (root.FRAGVIZ.VIEWERS) to register, find and invoke viewer per mime type
 */
(function(){

    var root = this;
    var viewers = {};

    // fake the namespace definition
    /**
     * Global Fragments Visualization namespace
     * @namespace FRAGVIZ
     */
    if (typeof root.FRAGVIZ === "undefined") root.FRAGVIZ = {
        styles : {
            include : "http://localhost:9000/styles/include.css",
        }
    };

    /**
     * Models a resource defined by a URI, a fragment and mimetype
     * @class RESOURCE
     * @memberof FRAGVIZ
     */
    var RESOURCE = root.FRAGVIZ.RESOURCE = function(uri, fragment, mimeType) {
        _.extend(this, {
            srcBase: uri,
            srcFrag: fragment,
            parsedMimeType: mimeType
        })
    }

    OpenLayers.Map.prototype.addLayerWithExtent = function (layer) {
        this.addLayer(layer)

        var bbox = layer.getDataExtent && layer.getDataExtent()
        if (bbox) {
            if (this.getExtent()) this.getExtent().extend(bbox)
            else this.zoomToExtent(bbox)
        }
        else {
            var firstExtent = false
            var _this = this
            layer.events.register(
                "loadend",
                layer,
                function (e) {
                    if (!firstExtent) {
                        var bbox = e && e.object && e.object.getDataExtent && e.object.getDataExtent()
                        if (bbox)
                            if (_this.getExtent()) this.getExtent().extend(bbox)
                            else _this.zoomToExtent(bbox)
                        else
                            _this.zoomToMaxExtent()
                        firstExtent = true
                    }
                })
        }

    },


        _.extend(RESOURCE,
            /** @lends FRAGVIZ.RESOURCE */ {
                /**
                 * Builds a Resource from a URI (extracting mimetype and fragment from the URI)
                 * @param uri
                 * @param mimeType
                 * @returns {FRAGVIZ.RESOURCE}
                 */
                FROMURI : function(uri, mimeType) {
                    var parsedUri = FRAGVIZ.UTILS.URI.parseUri(uri)
                    var srcBase = parsedUri.unfragmented
                    var srcFrag = parsedUri.hash && FRAGVIZ.UTILS.parseFragment('#'+parsedUri.hash)

                    if (!srcBase || srcBase.length == 0) FRAGVIZ.LOG.warn("Link with no src base: "+ uri)
                    return new RESOURCE(srcBase, srcFrag, mimeType)
                }
            })


    _.extend(RESOURCE.prototype,
        /** @lends FRAGVIZ.RESOURCE.prototype */
        {
            /**
             * @returns the fragment Object
             */
            getSrcFragment: function() {return this.srcFrag},

            /**
             * @returns the resource URL, without the fragment
             */
            getSrcURI: function() {return this.srcBase},

            /**
             * The mime type is guessed if needed from the URI extension
             * @returns the resource mime type
             */
            getSrcMimeType: function() {
                return this.parsedMimeType ||
                    (this.parsedMimeType = FRAGVIZ.UTILS.MIME.getExtensionMimeType(FRAGVIZ.UTILS.URI.parseUri(this.getSrcURI()).extension))
            }
        })

    /** @namespace FRAGVIZ.VIEWERS */
    var VIEWERS = root.FRAGVIZ.VIEWERS = {

        defaultBasemaps : [
            // default basemap
            {
                "title": "OSM",
                "type" : "XYZ",
                "url" : "http://tile.openstreetmap.org/{z}/{x}/{y}.png",
                "attribution" : " Map tiles & Data by OpenStreetMap, under CC BY SA."
            }
        ],

        /**
         * Find an adequate viewer for the given uri and mimetype, from the provided list of viewers.
         * List of viewers can either be an array, or a map with mimetypes as keys.
         * Matching priority is
         *   1. mimeType
         *   2. uri
         *   3. extracted mimetype from uri
         *   4. Content-type from HEAD request
         *
         * @param uri
         * @param mimeType
         * @param viewerMap optional map or array; defaults to embedded list of viewers
         * @param useHttpHead if true, an HTTP HEAD request may be done in last resort to fetch the mime type
         * @memberof FRAGVIZ.VIEWERS
         */
        findMatchingViewer: function(uri, mimeType, viewerMap, useHttpHead) {
            var matchingViewer;

            viewerMap = viewerMap || viewers;

            //TODO can viewerMap be an array? looks like it's always a map
            if ($.isArray(viewerMap)) {
                // check viewers that have a MimeType match policy
                matchingViewer = _.find(
                    viewerMap,
                    function(viewer) { return viewer.acceptsMimeType && viewer.acceptsMimeType(mimeType) }
                );
            } else if (mimeType && mimeType != '') {
                matchingViewer = viewerMap[mimeType] || viewerMap["DEFAULT"];
            }

            if (! matchingViewer && (!mimeType || mimeType == '')) {
                // no viewer found, and no mime type provided

                // check viewers that have a URI match policy
                matchingViewer = _.find(
                    viewerMap,
                    function(viewer) { return viewer.acceptsURI && viewer.acceptsURI(uri) }
                );

                if (matchingViewer)
                // viewer matches --> store resulting mime type
                    mimeType = matchingViewer.acceptsURI(uri)
                else {
                    // still no viewer --> extract mimetype from extension
                    mimeType = FRAGVIZ.UTILS.MIME.getExtensionMimeType(FRAGVIZ.UTILS.URI.parseUri(uri).extension);

                    if ($.isArray(viewerMap)) {
                        // check viewers that have a MimeType match policy
                        matchingViewer = _.find(
                            viewerMap,
                            function(viewer) { return viewer.acceptsMimeType && viewer.acceptsMimeType(mimeType) }
                        );
                    } else {
                        matchingViewer = viewerMap[mimeType];
                    }
                }
            }

            var matchingViewerPromise = $.Deferred()

            if (matchingViewer) {
                matchingViewerPromise.matchedMimeType = mimeType
                matchingViewerPromise.resolve(matchingViewer)
            } else if (useHttpHead) {
                var _this = this;

                // HTTP HEAD to peek at mime type
                $.ajax(FRAGVIZ.UTILS.proxifyUrl(uri), {
                    type: "HEAD",
                    async: true
                }).done(function (message, text, jqXHR) {
                    var headMimeType = jqXHR.getResponseHeader('Content-Type')

                    // call findMatchingViewer again on this mimetype, without useHttpHead
                    _this.findMatchingViewer(uri, headMimeType, viewerMap, false)
                        .done (function(viewer) {
                        matchingViewerPromise.matchedMimeType = headMimeType
                        matchingViewerPromise.resolve(viewer)
                    })
                })
            } else {
                FRAGVIZ.LOG.warn("No specific viewer found for ["+mimeType+"] "+uri)
                matchingViewerPromise.matchedMimeType = mimeType
                matchingViewerPromise.resolve(viewerMap["DEFAULT"])
            }

            return matchingViewerPromise;
        },

        /**
         * Find an adequate viewer and embed an instance of it in the provided container.
         * Container will have a 'curViewer' reference to the new viewer
         * @param container required; will hold the viewer instance
         * @param uri required; the resource to display
         * @param mimeType optional
         * @memberof FRAGVIZ.VIEWERS
         * @deprecated
         */
        display: function(container, uri, mimeType, fragment) {
            FRAGVIZ.LOG.warn("displayResource should be used instead")
            this.displayResource(
                container,
                {   srcBase: uri,
                    srcFrag: fragment,
                    parsedMimeType: mimeType})
        },

        /**
         * Find an adequate viewer and embed an instance of it in the provided container.
         * Container will have a 'curViewer' reference to the new viewer
         * Returns a promise on the viewer that will be resolved once the resource is displayed
         * @param container required; will hold the viewer instance
         * @param resource required; the resource to display
         * @param viewerOptions properties to be overlaid on created viewer
         * @memberof FRAGVIZ.VIEWERS
         */
        displayResource: function(container, resource, viewerOptions) {

            var cont = $(container)
            var curViewer = cont.prop('curViewer');
            var viewerPromise = $.Deferred()

            if (!resource) {
                curViewer && curViewer.destroy();
                cont.prop('curViewer', undefined)
                viewerPromise.resolve(false)
            } else {

                var mimeType = resource.getSrcMimeType();

                // store it in resource, or simply pass it down below ?
                resource.parsedMimeType = mimeType

                var viewerConsPromise = VIEWERS.findMatchingViewer(resource.getSrcURI(), mimeType, viewers, true);

                viewerConsPromise.done(function (viewerCons) {
                    if (!curViewer || curViewer.cons != viewerCons) {
                        if (curViewer) curViewer.destroy();
                        // no viewer yet --> build it
                        curViewer = viewerCons(container, viewerOptions);
                        //set cons on instance to check for equivalence above
                        curViewer.cons = viewerCons;
                        curViewer.matchedMimeType = viewerConsPromise.matchedMimeType;
                        FRAGVIZ.LOG.info("Viewer selected for " + resource.getSrcURI() + " [" + mimeType + "] : " + curViewer);
                    }

                    $(curViewer.container).attr("data-vieweruri", resource.getSrcURI())
                    curViewer.displayResource(resource);

                    // currently always resolve to true - TODO return false if failure
                    viewerPromise.resolve(curViewer)
                })
            }

            return viewerPromise;
        },

        /**
         * Register a viewer prototype.
         * The prototype gets extended with {@link FRAGVIZ.VIEWERS.viewer} prototype as it gets registered.
         * @param name display name of the viewer
         * @param mimeType supported mime type(s), as a string or array of strings
         * @param prototype
         * @memberof FRAGVIZ.VIEWERS
         */
        registerPrototype: function(name, mimeType, prototype) {

            var mimeTypes = $.isArray(mimeType) && mimeType || [mimeType]

            // register all mime types in UTILS if they don't exist
            _.each(mimeTypes, function(mt) {
                if (!FRAGVIZ.UTILS.MIME.getMimeExtension(mt))
                // use last element of mime type as extension
                    FRAGVIZ.UTILS.MIME.registerMimeExtension(mt.split('/').splice(-1)[0], mt)
            })

            // the acceptor that will be used to check whether this viewer instance or constructor is matching
            var mimeTypeAcceptor = function(mt) {
                return _.find(mimeTypes, function(val) {return val == mt;}) ;
            };

            var viewerClass = FRAGVIZ.VIEWERS[name] = function(name, instanceProps) {
                return FRAGVIZ.VIEWERS.viewer.apply(this,arguments)
            }

            _.extend(viewerClass.prototype, FRAGVIZ.VIEWERS.viewer.prototype , prototype || {})

            var newViewerCons = function(container, viewerOptions) {

                var $cont = $(container)
                if ($cont.prop('curViewer')) $cont.prop('curViewer').destroy();

                var newViewer = new viewerClass(name, {} /* TODO pass instance props ? */);

                newViewer.container = $cont[0];
                newViewer.acceptsMimeType = mimeTypeAcceptor;
                $cont.prop('curViewer', newViewer);

                newViewer.initViewer(viewerOptions)

                return newViewer;
            };
            // set the viewer name on the constructor, for reference
            newViewerCons.viewerName = name

            if (prototype.acceptsURI) newViewerCons.acceptsURI = function(uri) {return prototype.acceptsURI(uri)};
            newViewerCons.acceptsMimeType = mimeTypeAcceptor;

            $.each(mimeTypes, function(idx,val) {viewers[val] = newViewerCons;})
        },
    };



    /**
     * Abstract model for a component that can display annotations adequately
     */
    VIEWERS.AnnotationViewer = function() {

    };
    VIEWERS.AnnotationViewer.prototype = {
        /**
         * Annotation must be of the form
         * <pre>
         * {
         *   fragment: {
         *      hash:
         *      isTo:
         *   },
         *   description: ,
         *   name: ,
         * }
         * </pre>
         * @param annotation
         */
        displayAnnotation: function(annotation) {

        },

        clearAnnotationDisplay: function() {

        },

        destroy: function() {
            this.clearAnnotationDisplay();
        } ,

        refreshAnnotations: function() {
            FRAGVIZ.LOG.warn("refreshAnnotations is deprecated")
        },

        toggleAnnotationHighlight: function(annotationId, toggle) {

        }

    }

    var annotationViewerClass = VIEWERS.AnnotationViewer /*typeof FRAGVIZ !== "undefined" && FRAGVIZ.UI && FRAGVIZ.UI.AnnotationViewer*/


    /**
     * Renders annotations.
     * @class FRAGVIZ.VIEWERS.AnnotationRenderer
     * @abstract
     */
    VIEWERS.AnnotationRenderer = function() {

    };
    _.extend(VIEWERS.AnnotationRenderer.prototype,
        /** @lends FRAGVIZ.VIEWERS.AnnotationRenderer */
        {
            renderAnnotationInDiv: function(annotation, $annotBox, viewer) {
                $annotBox.append(this.renderDetailsContent(annotation, viewer))
                $annotBox.append(this.renderActionBarContent(annotation, viewer))

                var clickFn = this.createClickListener(annotation, viewer)
                clickFn && $annotBox.click(clickFn)

                var hoverFn = this.createHoverListener(annotation, viewer)
                hoverFn && $annotBox.hover(
                    function(e) {
                        hoverFn(true, e)
                    },
                    function(e) {
                        hoverFn(false, e)
                    }
                )
            },

            /* Return (set of) HTML elems to be appended to the action bar container */
            renderActionBarContent: function(annotation, viewer) {

            },

            /* Return (set of) HTML elems to be appended to the details container */
            renderDetailsContent: function(annotation, viewer) {
                // make and append div containg the details of the annotation
                var detailsDiv = $("<div></div>").addClass("details")
                detailsDiv.html("<b>"+annotation.name+"</b><br><span>"+annotation.description+"</span>")

                // make and append link icon
                var linkIcon = $("<div></div>").addClass("linkIcon32")

                return [detailsDiv, linkIcon]
            },

            /* returns a listener function that takes the event as arg */
            createClickListener: function(annotation, viewer) {

            },

            /* returns a listener function that takes (bool hoverIn, event) as args */
            createHoverListener: function(annotation, viewer) {

            },

            renderCapturedFragment: function(fragment, $elem, viewer) {
                var addIcon = $("<div class='actionBar'></div>")
                $elem.append(addIcon)
                addIcon
                    .addClass("addLink")
                    .click(function() {
                        viewer.onFragmentCapture(fragment)
                    }
                )
            }
        }
    )



    /**
     * Creates a Viewer instance, extended with the given instance props
     * @param name
     * @param instanceProps
     * @class FRAGVIZ.VIEWERS.viewer
     * @abstract
     */
    VIEWERS.viewer = function(name, instanceProps) {

        if (annotationViewerClass) annotationViewerClass.apply(this)
        $.extend(this,
            {
                currentMimeType: undefined,
                currentURI: undefined,
                currentFragment: undefined,
                container:null,         // the HTML container where to create the viewer
                name:name || "Unnamed viewer",  // a display name for the viewer
                eventListener:null,     // a listener where to send browsing events
                selectionListener:null, // sends select events when something is selected
                isReady: false,
                readyCallbacks: [],
                annotationRenderer : new VIEWERS.AnnotationRenderer()
            },
            instanceProps);
    };

    /**
     * Define the VIEWERS.viewer prototype, as an extension of AnnotationViewer.
     */
    _.extend(VIEWERS.viewer.prototype, annotationViewerClass ? annotationViewerClass.prototype : {},
        /** @lends FRAGVIZ.VIEWERS.viewer */
        {

            initViewer: function(viewerOptions) {
                // TODO something smarter
                if (viewerOptions)
                    _.extend(this, viewerOptions)
            },

            displayError: function(err) {
                var errDiv = $("<div class='error'></div>").appendTo($(this.container))
                errDiv.text(err)
            },

            /**
             * To be overridden.
             * Display the resource in the current viewer
             * @param resource
             */
            displayResource: function(resource, refresh) {
                //REFACTOR-DONE resource
                // here we update the mimetype if absent TODO: should be done in Resource Constructor
                var mimeType = resource.getSrcMimeType()
                var fragment = resource.getSrcFragment()

                //TODO is this enough of a check? what if different resource with same uri?
                if (resource.getSrcURI() != this.currentURI || mimeType != this.matchedMimeType || refresh) {
                    this.currentResource = resource;
                    // new uri --> display then go to fragment
                    var $this = this;
                    // if there's a fragment, create a callback to display fragment
                    var callback = fragment?
                        function(){
                            $this.gotoFragment(fragment)
                        } : undefined;

                    this.displayBase(resource.getSrcURI(), mimeType, callback);
                } else {
                    if (fragment) this.gotoFragment(fragment);
                }

                $(this.container).addClass('fragmentsContainer');
                var hoverMenu = $(this.container).find('.hoverMenu');
                if (!hoverMenu || hoverMenu.length == 0) {
                    $(this.container)
                        .prepend($("<div style='position: relative'></div>").append(hoverMenu = $("<div></div>")))
                    this.renderHoverMenu(hoverMenu);
                }

            },

            renderHoverMenu : function($menuDiv) {
                var _cont = this.container;
                $menuDiv
                    .addClass('hoverMenu widget')
                    .append(
                    $("<i class='glyphicon-share glyphicon'></i>")
                        .click(function(){
                            window.open(_cont.curViewer.currentURI, '_blank')
                        })
                )
            },

            loading : function(isLoading) {
                if (typeof isLoading === 'undefined' )
                    return this.isLoading
                else
                    this.isLoading = isLoading
            },

            displayBase: function(baseUri, mimeType, callback) {
                this.setReady(false);
                this.loading(true);
                this.currentURI = baseUri;
                this.currentMimeType = mimeType;
            },

            /**
             * Move the viewer to a different container, while retaining the current state
             * @param container
             */
            moveToContainer: function(container) {
                var oldContainer = this.container;
                this.container = container;
                $(container).prop('curViewer', this);

                if (oldContainer) {
                    $(oldContainer).empty().removeProp('curViewer');
                }
            },

            /**
             * Destroy the viewer and all its dependencies
             */
            destroy: function() {
                if (this.container) {
                    $(this.container).empty().removeProp('curViewer');
                    this.container = undefined;
                }

                if (annotationViewerClass) annotationViewerClass.prototype.destroy.apply(this, arguments);
            },

            toString: function() {
                return this.name;
            },

            /**
             * Focus on the selected fragment
             * Fragment must either be the full URL, or the fragment part beginning with #
             * @param uriFragment
             */
            gotoFragment: function(uriFragment) {
                this.currentFragment = this.getParsedFragment(uriFragment);
            },

            getParsedFragment: function(fragment) {
                if (typeof fragment == "object" && fragment.hash) return fragment;
                else if (typeof fragment == "string") return FRAGVIZ.UTILS.parseFragment(fragment)
                else return undefined
            },

            /* @deprecated */
            captureFragment: function(callback) {

            },

            allowFragmentCapture: function(callback) {
                this.onFragmentCapture = callback
            },

            clearCapturedFragment: function() {},

            pause: function() {

            },

            setAnnotationList: function(annotationList) {
                // call the actual setAnnotations only when viewer is ready
                this.onReady(function(viewer) {
                    // call parent func
                    if (annotationViewerClass) annotationViewerClass.prototype.setAnnotationList.apply(viewer, [annotationList]);
                });

            },

            getAnnotationRenderer: function() {
                return this.annotationRenderer;
            },



            /**
             *  Returns the content of the annotatin, oriented with the current viewer's resource as the source :
             *  {
             *     url :  ,
             *     fragment :  ,
             *     isTo : ,
             *     linkedResource : {
             *       url :
             *       fragment :
             *     }
             *  }
             *  If both to/from uris target this viewer's resource, 'from' is considered the source
             * @param annotation
             */
            getDirectedPair: function(annotation) {
                return FRAGVIZ.UTILS.getDirectedPair(annotation, this.currentURI.toString())
            },


            /*
             getAnnotationDirections: function(annotation) {
             // watch out, annotations are trimmed by default when added (removing trailing slashes)
             var location = FRAGVIZ.UTILS.URI.parseUri(this.currentURI.toString()).unfragmented
             var result = {}
             if (annotation.fromUri == location) {
             result.from = annotation.fragment
             }
             if (annotation.toUri == location) {
             result.to =  annotation.toFragment
             }
             if (!result.from && !result.to) {
             return undefined
             }

             return result
             },

             getAnnotationRelatedFragment: function(annotation) {
             var fragment

             // check whether this viewer matches the annotation src or target
             var fromToFrags = this.getAnnotationDirections(annotation)
             if (!fromToFrags) return

             //TODO if both 'from' and 'to' are present, it defaults to 'from'. Correct ?
             if (fromToFrags.from) {
             fragment = fromToFrags.from
             fragment.isFrom = true
             }
             else if (fromToFrags.to) {
             fragment = fromToFrags.to
             fragment.isTo = true
             }
             return fragment

             },
             */

            /**
             * Register a callback on the viewer 'ready' event
             * @param callback
             */
            onReady: function(callback) {
                if (this.isReady) callback(this);
                else this.readyCallbacks.push(callback);
            },

            setReady: function(ready) {
                this.isReady = ready;
                var _this = this;
                if (this.isReady) {
                    _.each(this.readyCallbacks, function(callback) {
                        callback(_this);
                    });
                    this.readyCallbacks = [];

                    this.loading(false);
                }
            }
        });

    /**
     * Register the default viewer
     */
    //TODO remove and add DEFAULT in supported mimetypes of HTML viewer
    /*
     VIEWERS.registerPrototype(
     "Default Viewer",
     ["DEFAULT"],
     {   viewFrame: null,
     useProxy: true,
     display: function(uri) {

     // call parent func
     VIEWERS.viewer.prototype.display.apply(this, arguments);

     var $frame = this.viewFrame = $('<iframe class="displayFrame"></iframe>')[0];
     var $listener = this.selectionListener;

     $(this.container).empty().append(this.viewFrame);
     FRAGVIZ.UTILS.loadIntoFrame(
     uri, this.viewFrame, this.useProxy,
     function() {
     $($frame.contentDocument).on('textSelect',function(e) {
     $listener();
     });
     });
     },

     destroy: function() {
     if (this.viewFrame) {
     $(this.viewFrame).remove();
     }

     // call parent func
     VIEWERS.viewer.prototype.destroy.apply(this, arguments);
     }
     });
     */


    VIEWERS.registerPrototype(
        "HTML Viewer",
        ["text/html", "text/plain", "DEFAULT"],
        /**
         * Plain iframe-based viewer for natively supported resources (HTML, text, ...)
         * @class FRAGVIZ.VIEWERS.HTMLViewer
         * @extends FRAGVIZ.VIEWERS.viewer
         */
        {
            initViewer: function(viewerOptions) {
                this.viewFrame = null
                this.useProxy = true
                VIEWERS.viewer.prototype.initViewer.apply(this, arguments);
                this.annotBoxes = {};
            },

            loading:function (isLoading) {

                // call parent func
                VIEWERS.viewer.prototype.loading.apply(this, arguments);

                if (this.isLoading) {
                    $(this.container).find(".loader").show()
                } else {
                    $(this.container).find(".loader").hide()
                }
            },

            displayBase:function (uri, mimeType, callback) {

                // call parent func
                VIEWERS.viewer.prototype.displayBase.apply(this, arguments);

                var containerDiv = $("<div class='viewerContainer htmlViewerContainer'></div>")
                var frameContainer = $("<div class='frameContainer' style='position: relative'></div>").appendTo(containerDiv)
                $(this.container)
                    .append(containerDiv)
                    .append("<div class='loader'>Loading</div>") // add a loading div

                if (!this.viewFrame) {
                    this.viewFrame = $('<iframe class="displayFrame" name="displayFrame"></iframe>')[0];
                    $(frameContainer).empty().append(this.viewFrame);
                }
                var $frame = this.viewFrame;
                var $listener = this.selectionListener;
                var _this = this;

                FRAGVIZ.UTILS.loadIntoFrame(
                    uri, this.viewFrame, this.useProxy,
                    function () {
                        var scriptingAllowed
                        try {
                            $frame.contentDocument
                            scriptingAllowed = true
                        } catch (err) {
                            scriptingAllowed = false
                        }

                        if (scriptingAllowed) {
                            $($frame.contentDocument.body).on('textSelect', function (e) {
                                if ($listener) $listener(e);
                            });
                            var $frameHtml = $('html', $frame.contentDocument)
                            $($frame).css('height', $frameHtml[0].offsetHeight+'px')
                            $($frame).css('width', $frameHtml[0].offsetWidth+'px')

                            $frameHtml.css('overflow', 'hidden')
                            $('head', $frame.contentDocument).append(
                                $('<link rel="stylesheet" type="text/css" href="' + root.FRAGVIZ.styles.include + '" >', $frame.contentDocument));
                        } else {
                            FRAGVIZ.LOG.warn("Cross-site scripting not allowed for "+uri+". Consider activating the proxy.")
                        }

                        _this.setReady(true);
                        if (callback) callback();
                    });


            },

            moveToContainer:function (container) {

                // call parent func
                VIEWERS.viewer.prototype.moveToContainer.apply(this, arguments);

                if (this.viewFrame) {
                    $(container).empty().append(this.viewFrame);
                    this.displayResource(this.currentResource, true);
                }
            },

            destroy:function () {
                if (this.viewFrame) {
                    $(this.viewFrame).remove();
                }

                // call parent func
                VIEWERS.viewer.prototype.destroy.apply(this, arguments);
            },

            /***
             *
             */
            gotoFragment:function (fragment) {
                VIEWERS.viewer.prototype.gotoFragment.apply(this, arguments);
                fragment = this.currentFragment

                if (!fragment.hash) return;

                if (fragment.hash.xpath) {
                    var selRange = FRAGVIZ.UTILS.getRangeFromXPathFragment(
                        fragment.hash.xpath[0].startXpath,
                        fragment.hash.xpath[0].startOffset,
                        fragment.hash.xpath[0].endXpath,
                        fragment.hash.xpath[0].endOffset,
                        this.viewFrame.contentDocument
                    );

                    FRAGVIZ.UTILS.setSelectionRange(this.viewFrame, selRange);
                    $(this.container).find(".htmlViewerContainer")[0].scrollTo(0, FRAGVIZ.UTILS.computeTotalOffset(selRange.startContainer)[1] - 30);
                    //if (selRange.startContainer.scrollIntoView) selRange.startContainer.scrollIntoView();
                    //else if (selRange.startContainer.parentNode) selRange.startContainer.parentNode.scrollIntoView();
                }
            },

            clearCapturedFragment: function() {
                // this will set onFragmentCapture
                VIEWERS.viewer.prototype.clearCapturedFragment.apply(this, arguments);

                if (this.capturedFragmentActions)
                    $(this.capturedFragmentActions).remove()
            },

            allowFragmentCapture: function(callback) {
                // this will set onFragmentCapture
                VIEWERS.viewer.prototype.allowFragmentCapture.apply(this, arguments);

                var _this = this;
                if (_this.onFragmentCapture) {
                    this._mouseUpListener = function(evt) {
                        if (_this.capturedFragmentActions &&  $.contains(_this.capturedFragmentActions[0], evt.target))
                            return true

                        _this.captureFragment(function(fragment) {

                            _this.clearCapturedFragment();

                            if (fragment.hash && fragment.hash.xpath) {
                                var bbox = _this.fragment2absCoords(fragment)

                                _this.capturedFragmentActions = $("<div></div>")
                                var annotationLayer = _this.getAnnotationLayer()

                                $(annotationLayer).append(_this.capturedFragmentActions);

                                _this.getAnnotationRenderer().renderCapturedFragment(fragment, _this.capturedFragmentActions, _this);

                                _this.capturedFragmentActions
                                    .css("left", (bbox[2]-18)+'px')
                                    .css("top", (bbox[1]-20)+'px')
                                    .css("position", 'absolute')
                            }

                        })

                        return true // make sure to return true to continue event propagation
                    }

                    $('body', this.viewFrame.contentDocument).mouseup(this._mouseUpListener)
                } else {
                    if (this._mouseUpListener)
                        $('body', this.viewFrame.contentDocument).unbind('mouseup', this._mouseUpListener)
                }
            },


            getAnnotationLayer: function() {

                var annotationLayer = this.annotationLayer
                if (!annotationLayer) {
                    annotationLayer = this.annotationLayer = $("<div class='annotationLayer'></div>").appendTo($(this.container).find(".frameContainer"))[0]
                }

                return annotationLayer
            },

            fragment2absCoords : function (fragment) {
                var selRange = FRAGVIZ.UTILS.getRangeFromXPathFragment(
                    fragment.hash.xpath[0].startXpath,
                    fragment.hash.xpath[0].startOffset,
                    fragment.hash.xpath[0].endXpath,
                    fragment.hash.xpath[0].endOffset,
                    this.viewFrame.contentDocument
                );


                var bbox = FRAGVIZ.UTILS.computePreciseRangeXywh(selRange);

                return bbox;
            },

            captureFragment: function(callback) {
                var sel = FRAGVIZ.UTILS.getSelectionRange(this.viewFrame.contentWindow);
                if (sel && (sel.startOffset != sel.endOffset || sel.startContainer != sel.endContainer)) {
                    var xpath = FRAGVIZ.CODECS.xpathCodec.getXPathFragmentFromRange(sel)
                    callback({hash:{xpath:[xpath]}})
                    var recomputedRange = FRAGVIZ.CODECS.xpathCodec.getRangeFromXPathFragment(xpath, this.viewFrame.contentDocument)
                    FRAGVIZ.UTILS.setSelectionRange(this.viewFrame.contentWindow, recomputedRange);
                }
            },

            clearAnnotationDisplay: function() {
                for (var annot in this.annotBoxes) {
                    $(this.annotBoxes[annot]).remove();
                }
            },

            //REFACTOR-DONE annotations
            displayAnnotation: function(annotation) {
                if (!this.viewFrame.contentDocument) FRAGVIZ.LOG.warn("Trying to display annotations before document is loaded");

                var directedPair = this.getDirectedPair(annotation);

                if (directedPair.fragment.hash && directedPair.fragment.hash.xpath) {
                    var bbox = this.fragment2absCoords(directedPair.fragment)

                    var annotBox = $("<div><div></div></div>");
                    $(this.getAnnotationLayer()).append(annotBox);
                    // class must be added after appending to be taken into account

                    this.getAnnotationRenderer().renderAnnotationInDiv(annotation, annotBox, this);

                    annotBox[0].srcAnnot = annotation;

                    // add annotation DIV to list of displayed annotations
                    this.annotBoxes[annotation] = annotBox[0];

                    var padding = 2;

                    annotBox.css("top", (bbox[1]-padding)+'px')
                        .css("left", (bbox[0]-padding)+'px')
                        .css("width", (bbox[2]-bbox[0]+(2*padding))+'px')
                        .css("height", (bbox[3]-bbox[1]+(2*padding))+'px')
                        .addClass("annot "+ (directedPair.isTo?"toAnnot":"fromAnnot"));

                    return annotBox
                }
            },


            toggleAnnotationHighlight: function(annotationId, toggle) {

                // removed as there's no longer annotationList
                //if (typeof annotation == "string") annotation = this.annotationList.getById(annotation);

                if (annotationId && this.annotBoxes[annotationId]) {
                    var $annotBox = $(this.annotBoxes[annotationId])
                    if (toggle === undefined) toggle = !$annotBox.hasClass("active");

                    if (toggle) $annotBox.addClass("active")
                    else $annotBox.removeClass("active")
                }
            },

            refreshAnnotations: function() {
                if (annotationViewerClass) annotationViewerClass.prototype.refreshAnnotations.apply(this);

                $(this.viewFrame)
                    .scrolllinks({
                        anchorSelector: '.annot',
                        content: function (i, $page) {
                            return '<span class="scrolllink">' +( $page[0].srcAnnot.description || $page[0].srcAnnot.name)+ '</span>';
                        }
                    });
                //TODO append a full-size background transparent overlay with a hover-on effect
            }
        });

    VIEWERS.registerPrototype(
        "PDF Viewer",
        ["application/pdf"],
        /**
         * PDF.js based viewer
         * @class FRAGVIZ.VIEWERS.PDFViewer
         * @extends FRAGVIZ.VIEWERS.viewer
         */
        {
            pdfCanvas: null,
            pdf: null,
            useProxy: true,
            scale: 1.5,

            initViewer: function(viewerOptions) {
                VIEWERS.viewer.prototype.initViewer.apply(this, arguments);
                this.annotBoxes = {};

                var loadingDiv = $("<div class='loader'>Loading</div>").appendTo($(this.container))
            },

            loading:function (isLoading) {

                // call parent func
                VIEWERS.viewer.prototype.loading.apply(this, arguments);

                if (this.isLoading) {
                    $(this.container).find(".loader").show()
                } else {
                    $(this.container).find(".loader").hide()
                }
            },

            displayBase:function (uri, mimeType, callback) {
                // call parent func
                VIEWERS.viewer.prototype.displayBase.apply(this, arguments);

                var proxyUri = this.useProxy ? FRAGVIZ.UTILS.proxifyUrl(uri) : uri

                var containerDiv = $("<div class='viewerContainer pdfViewerContainer'></div>")
                var viewerDiv = $("<div class='pdfViewer'></div>").appendTo(containerDiv)
                $(this.container).append(containerDiv)

                this.viewer = new PDFJS.PDFViewer({
                    container: containerDiv[0],
                    viewer: viewerDiv[0]
                })
                var _this = this
                this.pdf = PDFJS.getDocument(proxyUri)
                this.pdf.then(function(doc){
                    _this.viewer.setDocument(doc)
                    _this.viewer.pagesPromise.then(function() {
                        //_this.viewer.currentScaleValue = 'auto'
                        _this.setReady(true);

                        /*
                         window.addEventListener('resize', function webViewerResize(evt) {
                         _this.viewer.currentScaleValue = 'auto'
                         _this.viewer.update();
                         $(".pdfViewer .annotationLayer").css({transform: 'scale('+_this.viewer.currentScale+')'})
                         });
                         */

                        callback && callback()
                    })
                })
                this.pdf.catch(function (error) {
                    _this.displayError(error)
                    _this.setReady(true);
                })

                /*
                 if (!this.pdfCanvas) {
                 $(this.container).append(
                 $("<div class='pdfPage'></div>")
                 .append(this.pdfCanvas = $("<canvas></canvas>")));
                 }


                 this.pdf = PDFJS.getDocument(proxyUri)
                 this.gotoPage(1)
                 */

                var $listener = this.selectionListener;

            },

            clearCapturedFragment: function() {
                // this will set onFragmentCapture
                VIEWERS.viewer.prototype.clearCapturedFragment.apply(this, arguments);

                if (this.capturedFragmentActions)
                    $(this.capturedFragmentActions).remove()
            },

            allowFragmentCapture: function(callback) {
                // this will set onFragmentCapture
                VIEWERS.viewer.prototype.allowFragmentCapture.apply(this, arguments);

                var _this = this;
                if (_this.onFragmentCapture) {
                    this._mouseUpListener = function(evt) {
                        if (_this.capturedFragmentActions &&  $.contains(_this.capturedFragmentActions[0], evt.target))
                            return true

                        _this.captureFragment(function(fragment) {

                            _this.clearCapturedFragment();

                            if (fragment && fragment.hash && fragment.hash.xywh[0].w > 0 && fragment.hash.xywh[0].h > 0) {
                                var bbox = _this.fragment2viewportCoords(fragment)

                                _this.capturedFragmentActions = $("<div></div>")
                                var annotationLayer = _this.getAnnotationLayer(fragment.hash.page[0]-1)

                                $(annotationLayer).append(_this.capturedFragmentActions);

                                _this.getAnnotationRenderer().renderCapturedFragment(fragment, _this.capturedFragmentActions, _this);

                                _this.capturedFragmentActions
                                    .css("left", (bbox[2]-18)+'px')
                                    .css("top", (bbox[1]-20)+'px')
                                    .css("position", 'absolute')
                            }

                        })

                        return true // make sure to return true to continue event propagation
                    }

                    $(this.container).find(".pdfViewer").mouseup(this._mouseUpListener)
                } else {
                    if (this._mouseUpListener)
                        $(this.container).find(".pdfViewer").unbind('mouseup', this._mouseUpListener)
                }
            },


            destroy: function() {

                // call parent func
                VIEWERS.viewer.prototype.destroy.apply(this, arguments);
            },

            gotoPage: function(pageNb) {
                var _this = this
                return this.pdf && this.pdf.then(function(pdf) {
                    return pdf.getPage(pageNb).then(function(page) {
                        return _this.renderPage(page)})
                })
            },

            renderPage: function (page) {
                var viewport = page.getViewport(this.scale);

                var canvas = this.pdfCanvas[0]
                var context = canvas.getContext('2d');

                // The following few lines of code set up scaling on the context, if we are
                // on a HiDPI display.

                var outputScale = getOutputScale(context);
                canvas.width = (Math.floor(viewport.width) * outputScale.sx) | 0;
                canvas.height = (Math.floor(viewport.height) * outputScale.sy) | 0;
                context._scaleX = outputScale.sx;
                context._scaleY = outputScale.sy;
                if (outputScale.scaled) {
                    context.scale(outputScale.sx, outputScale.sy);
                }


                // The page, canvas and text layer elements will have the same size.
                canvas.style.width = Math.floor(viewport.width) + 'px';
                canvas.style.height = Math.floor(viewport.height) + 'px';


                // Painting the canvas...
                var renderContext = {
                    canvasContext: context,
                    viewport: viewport
                };
                var renderTask = page.render(renderContext);

                return renderTask.promise;
            },



            moveToContainer:function (container) {

                // call parent func
                VIEWERS.viewer.prototype.moveToContainer.apply(this, arguments);

                if (this.viewFrame) {
                    $(container).empty().append(this.viewFrame);
                    this.displayResource(this.currentResource, true);
                }
            },

            destroy:function () {
                if (this.viewFrame) {
                    $(this.viewFrame).remove();
                }

                // call parent func
                VIEWERS.viewer.prototype.destroy.apply(this, arguments);
            },

            /***
             *
             */
            gotoFragment:function (fragment) {
                VIEWERS.viewer.prototype.gotoFragment.apply(this, arguments);
                fragment = this.currentFragment

                if (!fragment.hash) return;

                if (fragment.hash.page) {
                    var xywh = fragment.hash.xywh &&
                        [   parseInt(fragment.hash.xywh[0].x),
                            parseInt(fragment.hash.xywh[0].y),
                            parseInt(fragment.hash.xywh[0].w),
                            parseInt(fragment.hash.xywh[0].h)]
                    this.viewer && this.viewer.scrollPageIntoView(
                        fragment.hash.page[0],
                        //xywh && ["frag",{name:'FitR'}, xywh[0], xywh[1], xywh[0]+xywh[2], xywh[1] + xywh[3]]

                        // simply scroll vertically, do not try to zoom on annotation box
                            xywh && ["frag",{name:'XYZ'}, xywh[0], xywh[1]+30, 'custom']

                    )
                }
            },


            captureFragment: function(callback) {
                var pageIndex = this.viewer.currentPageNumber - 1;
                var textLayer = this.viewer.getPageView(pageIndex).textLayer
                if (! $.contains(textLayer.textLayerDiv, window.getSelection().getRangeAt(0).commonAncestorContainer) &&
                    textLayer.textLayerDiv != window.getSelection().getRangeAt(0).commonAncestorContainer)
                // ignore events outside of text layer
                    return false

                var page = this.viewer.pages[pageIndex];
                var pageRect = page.canvas.getClientRects()[0];
                var selectionRects = window.getSelection().getRangeAt(0).getClientRects();
                var viewport = page.viewport;
                var rectToCoords = function (r) {
                    return viewport.convertToPdfPoint(r.left - pageRect.left, r.top - pageRect.top).concat(
                        viewport.convertToPdfPoint(r.right - pageRect.left, r.bottom - pageRect.top));
                }
                var selected = _.map(selectionRects, rectToCoords);

                // absolute bottom is 0, absolute top is pageRect.top
                var left, top, right, bottom;
                _.each(selected, function(rectCoords) {
                    left = left ? Math.min(left, rectCoords[0]) : rectCoords[0]
                    top = top ? Math.max(top, rectCoords[1]) : rectCoords[1]
                    right = right ? Math.max(right, rectCoords[2]) : rectCoords[2]
                    bottom = bottom ? Math.min(bottom, rectCoords[3]) : rectCoords[3]
                })

                var fragment = {hash:{
                    page: [pageIndex+1],
                    xywh:[ {x: Math.floor(left), y: Math.ceil(top), w: Math.ceil(right-left), h: Math.ceil(top-bottom)} ]
                }}
                callback(
                    fragment
                )
            },

            clearAnnotationDisplay: function() {
                for (var annot in this.annotBoxes) {
                    $(this.annotBoxes[annot]).remove();
                }
            },

            fragment2viewportCoords : function (fragment) {
                var xywh =
                    [   parseInt(fragment.hash.xywh[0].x),
                        parseInt(fragment.hash.xywh[0].y),
                        parseInt(fragment.hash.xywh[0].w),
                        parseInt(fragment.hash.xywh[0].h)]
                var pageView = this.viewer.getPageView(fragment.hash.page[0]-1)
                var bbox = pageView.viewport.convertToViewportRectangle(
                    [xywh[0],
                        xywh[1],
                            xywh[0] + xywh[2],
                            xywh[1] - xywh[3]]) // because pdf y coords are 0-bottom

                return bbox
            },

            getAnnotationLayer: function(pageIdx) {
                var pageView = this.viewer.getPageView(pageIdx)

                var annotationLayer = pageView.annotationLayer
                if (!annotationLayer) {
                    annotationLayer = pageView.annotationLayer = $("<div class='annotationLayer'></div>").appendTo(pageView.el)[0]
                }

                return annotationLayer
            },

            //REFACTOR-DONE annotations
            displayAnnotation: function(annotation) {
                if (!this.viewer) FRAGVIZ.LOG.warn("Trying to display annotations before document is loaded");

                var directedPair = this.getDirectedPair(annotation);

                if (!directedPair.fragment) {
                    // no fragment --> ignore
                    return;
                }

                if (directedPair.fragment.hash.page && directedPair.fragment.hash.xywh) {
                    var bbox = this.fragment2viewportCoords(directedPair.fragment)

                    var annotBox = $("<div><div></div></div>");
                    $(this.getAnnotationLayer(directedPair.fragment.hash.page[0]-1)).append(annotBox);
                    // class must be added after appending to be taken into account

                    this.getAnnotationRenderer().renderAnnotationInDiv(annotation, annotBox, this);

                    annotBox[0].srcAnnot = annotation;

                    // add annotation DIV to list of displayed annotations
                    this.annotBoxes[annotation] = annotBox[0];

                    var padding = 2;

                    annotBox.css("top", (bbox[1]-padding)+'px')
                        .css("left", (bbox[0]-padding)+'px')
                        .css("width", (bbox[2]-bbox[0]+(2*padding))+'px')
                        .css("height", (bbox[3]-bbox[1]+(2*padding))+'px')
                        .addClass("annot "+ (directedPair.isTo?"toAnnot":"fromAnnot"));

                    return annotBox
                }
            },


            toggleAnnotationHighlight: function(annotationId, toggle) {

                // removed as there's no longer annotationList
                //if (typeof annotation == "string") annotation = this.annotationList.getById(annotation);

                if (annotationId && this.annotBoxes[annotationId]) {
                    var $annotBox = $(this.annotBoxes[annotationId])
                    if (toggle === undefined) toggle = !$annotBox.hasClass("active");

                    if (toggle) $annotBox.addClass("active")
                    else $annotBox.removeClass("active")
                }
            },

            refreshAnnotations: function() {
                if (annotationViewerClass) annotationViewerClass.prototype.refreshAnnotations.apply(this);

                $(this.viewer.container)
                    .scrolllinks({
                        anchorSelector: '.annot',
                        content: function (i, $page) {
                            return '<span class="scrolllink">' +( $page[0].srcAnnot.description || $page[0].srcAnnot.name)+ '</span>';
                        }
                    });
                //TODO append a full-size background transparent overlay with a hover-on effect
            }
        });


    /**
     * @namespace FRAGVIZ.VIEWERS.YOUTUBE
     */
    VIEWERS.YOUTUBE =
    {
        /**
         * EXtracts the youtube ID from a URI
         * @param uri
         * @memberof VIEWERS.YOUTUBE
         */
        extractYoutubeId: function(uri) {
            var parsedURI = FRAGVIZ.UTILS.URI.parseUri(uri);
            if (parsedURI.protocol == 'youtube') return parsedURI.file;
            else if (parsedURI.host.indexOf('youtube.com') >= 0 && parsedURI.file == "watch") return parsedURI.queryKey.v[0];
            else if (parsedURI.host.indexOf('youtu.be') == 0) return parsedURI.file;
            else return undefined;
        }
    }


    /**
     * @namespace FRAGVIZ.VIEWERS.SOUNDCLOUD
     */
    VIEWERS.SOUNDCLOUD =
    {
        /**
         * EXtracts the soundcloud ID from a URI
         * @param uri
         * @memberof VIEWERS.SOUNDCLOUD
         */
        extractSoundcloudId: function(uri) {
            var parsedURI = FRAGVIZ.UTILS.URI.parseUri(uri);
            if (parsedURI.authority == 'soundcloud.com') return uri;
            else return undefined;
        }
    }


    VIEWERS.registerPrototype(
        "VideoJS Viewer",
        ["video/ogg", "video/mp4", "video/webm", "video/youtube", "video/vimeo", "audio/soundcloud", "audio/mp3"],
        /**
         * VideoJS-based viewer for videos
         * @class FRAGVIZ.VIEWERS.VideoJSViewer
         * @extends FRAGVIZ.VIEWERS.viewer
         */
        {
            video: null,
            annotDiv: null,
            player: null,
            annotationBar: null,

            initViewer: function(viewerOptions) {
                VIEWERS.viewer.prototype.initViewer.apply(this, arguments);
                this.annotBoxes = {};
            },

            // implement 'acceptURI' to declare it as an acceptance method for viewer registration
            acceptsURI: function(uri) {
                return (VIEWERS.SOUNDCLOUD.extractSoundcloudId(uri) && "audio/soundcloud") ||
                    (VIEWERS.YOUTUBE.extractYoutubeId(uri) && "video/youtube")||
                    (VIEWERS.VIMEO.extractVimeoId(uri) && "video/vimeo");
            },

            displayBase: function (uri, mimeType, callback) {

                // call parent func
                VIEWERS.viewer.prototype.displayBase.apply(this, arguments);

                var youtubeId = VIEWERS.YOUTUBE.extractYoutubeId(uri);
                var vimeoId = VIEWERS.VIMEO.extractVimeoId(uri);
                var soundcloudUrl = VIEWERS.SOUNDCLOUD.extractSoundcloudId(uri);

                this.annotDiv = $('<div class="annotations overlay"></div>')[0];
                this.video = $(
                    '<video class="video-js vjs-default-skin" width="100%" height="100%"></video>'
                )[0];

                var options = {
                    "controls": true,
                    "autoplay": true,
                    "preload": "auto"}

                if (mimeType == "video/youtube" || (!mimeType && youtubeId)) {
                    options.techOrder = ["youtube"]
                    options.src = uri
                }
                else if (mimeType == "video/vimeo" || (!mimeType && vimeoId)) {
                    options.techOrder = ["vimeo"]
                    options.src = uri
                }
                else if (mimeType == "audio/soundcloud" || (!mimeType && soundcloudUrl)) {
                    options.techOrder = ["soundcloud"]
                    options.soundcloud = {'source' : uri}
                }
                else
                    $(this.video).append($('<source src="' + uri + '" type=\'' + mimeType + '\' />'));

                $(this.container).empty()
                    //    .append(
                    //        $('<div style="display: inline-block;"></div>')
                    .append(this.video);

                this.player = _V_(this.video, options);

                this.player.el().style.width = "100%";
                this.player.el().style.height = "100%";

                // add the annotation overlay
                $(this.player.el()).append(this.annotDiv);

                var _viewer = this;

                this.player.on(
                    "timeUpdate",
                    function(args) {
                        // args is the time in sec --> ok
                        if (_viewer.eventListener) _viewer.eventListener(new FRAGVIZ.MODEL.TimeEvent(args));
                    });

                //duration is nto always set on 'ready'  --> use durationChange
                var initFn = function() {
                    var status = 0 // 0:waiting ; 1:in progress; 2:done
                    return function() {
                        if (status == 0) {
                            status = 1
                            try {
                                _viewer.setReady(true);
                                _viewer.refreshAnnotations();
                                if (callback) callback();
                                status = 2
                            } catch (err) {
                                FRAGVIZ.LOG.warn("Failed to init viewer", err)
                                status = 0
                            }
                        }
                    }
                } ()
                this.player.ready(function() {
                    // add a bar to hold annotation quick links
                    //$(".vjs-progress-holder", _viewer.video.parentNode)
                    $(this.controlBar.progressControl.el())
                        .append(_viewer.annotationBar = $("<div class='annotationBar'></div>")[0]);

                    if (this.duration() > 0) initFn()

                });

                this.player.on(
                    "durationchange",
                    function() {
                        initFn()
                        //if (_viewer.currentFragment) _viewer.gotoFragment(_viewer.currentFragment);
                    });
            },

            moveToContainer:function (container) {

                // call parent func
                VIEWERS.viewer.prototype.moveToContainer.apply(this, arguments);

                if (this.video) {
                    $(container).empty()
                        .append($(this.video.parentNode));
                }
            },

            pause: function() {
                if (this.player) this.player.pause();
            },

            destroy: function() {
                if (this.player) this.player.dispose();
                if (this.video) $(this.video.parentNode).remove();
                if (this.annotDiv) $(this.annotDiv).remove();

                // call parent func
                VIEWERS.viewer.prototype.destroy.apply(this, arguments);
            },

            gotoFragment: function(fragment) {
                VIEWERS.viewer.prototype.gotoFragment.apply(this, arguments);
                fragment = this.currentFragment

                if (fragment.hash && fragment.hash.t) {
                    // force the player to pause to make sure the play event is sent
                    this.player.pause();
                    this.player.one('play', function() {
                        // soundclound requires this to be done after play event
                        this.currentTime(fragment.hash.t[0].startNormalized);
                    })
                    this.player.play();

                }
            },

            captureFragment: function(callback) {
                callback({hash:{t:[{startNormalized:this.player.currentTime()}]}})
            },

            //REFACTOR-DONE annotations
            displayAnnotation: function(annotation) {
                var directedPair = this.getDirectedPair(annotation);
                if (directedPair.fragment && directedPair.fragment.hash && directedPair.fragment.hash.t) {
                    var  barLink = this.createBarQuickLink(annotation);

                    //$(barLink).tooltip({title: annotation.get('description')})
                    var tooltip = $("<div></div>").addClass("hl-tooltip").text(annotation.description)

                    $(barLink).append(tooltip)
                    /*
                     .hover(
                     function() {tooltip.show()},
                     function() {tooltip.hide()}
                     )
                     */
                    this.annotBoxes[annotation] = barLink
                    $(this.annotationBar).append(barLink);

                    return barLink
                }
            },

            refreshAnnotations: function() {
                if (!this.annotationBar) return;
                if (annotationViewerClass) annotationViewerClass.prototype.refreshAnnotations.apply(this);
            },

            clearAnnotationDisplay: function() {
                if (this.annotationBar) $(this.annotationBar).empty();
                if (this.annotDiv) $(this.annotDiv).empty();
            },

            //REFACTOR-DONE annotations
            createBarQuickLink: function(annotation) {
                // create multi bars ?
                var directedPair = this.getDirectedPair(annotation);
                var tHash = directedPair.fragment.hash.t;
                var span = tHash[0].endNormalized-tHash[0].startNormalized;
                var width = (100*span/this.player.duration())
                var offset = tHash[0].startNormalized*100/this.player.duration()
                return $("<div class='barQuickLink'></div>")
                    .click(function() {
                        FRAGVIZ.WORKSPACE.UI.getInstance().viewerSet.goto(annotation);
                    })
                    .css('width', width+'%')
                    .css('left', offset+'%');
            },

            toggleAnnotationHighlight: function(annotationId, toggle) {
                var link = this.annotBoxes[annotationId]
                // annotBoxes may not exist if resource was displayed without its annotations
                if (link) {
                    if (toggle === undefined) toggle = !$(link).hasClass("active")

                    // force the display
                    this.player.userActive(toggle)
                    link.toggleClass("active", toggle)
                }

            }
        });

    VIEWERS.VIMEO = {
        extractVimeoId: function(uri) {
            var parsedURI = FRAGVIZ.UTILS.URI.parseUri(uri);
            if (parsedURI.protocol == 'vimeo') return parsedURI.file;
            else if (parsedURI.host.indexOf('vimeo.com') == 0) return parsedURI.file;
            else if (parsedURI.host.indexOf('player.vimeo.com') == 0) return parsedURI.file;
            else return undefined;
        }
    }


    VIEWERS.registerPrototype(
        "D3 CSV Viewer",
        [], //["application/csv", "application/x-msdownload"],
        {
            chart: undefined,
            options: {
                firstLineHeaders: true,
                firstColumnHeaders: true,
                xHeaders: undefined,
                yHeaders: undefined,
                dataColumns: {from:2}
            },

            // implement 'acceptURI' to declare it as an acceptance method for viewer registration
            acceptsURI: function(uri) {
                // heuristics to match URLs such as worldbank CSV API
                return FRAGVIZ.UTILS.URI.parseUri(uri).queryKey.format == 'csv' && "application/csv"
            },

            displayBase:function (uri, mimeType, callback) {
                // call parent func
                VIEWERS.viewer.prototype.displayBase.apply(this, arguments);
                var _this= this;
                $.ajax(FRAGVIZ.UTILS.proxifyUrl(uri), {
                    // TODO doesn't work : OpenBelgium demo : FR headers to force metric system on wunderground weather
                    headers: {'Accept-Language' : 'fr-FR,fr;q=0.8,en;q=0.4'}
                })
                    .done(function(a) {
                        a = a.trim() // trim the result (remove occurring leading or trailing CRs, ...)
                        var data = d3.csv.parseRows(a)
                        _this.displayData(data);
                        _this.setReady(true);
                        if (callback) callback()
                    })
            },

            displayData: function(dataSeries) {
                if (this.chart) this.chart.destroy();

                /*
                 var actualData = [];
                 var _options = this.options;
                 var startCol = _options.dataColumns.from || (_options.firstColumnHeaders && 1) || 0;
                 var xHeaders = _options.xHeaders || (_options.firstLineHeaders && dataSeries[0]);
                 var yHeaders = _options.yHeaders;// || (_options.firstColumnHeaders && []);
                 _.each(
                 dataSeries.splice(_options.firstLineHeaders?1:0),
                 function(serie) {
                 var newSerie = _options.dataColumns.to?
                 {data: serie.splice(startCol, _options.dataColumns.to)}:
                 {data: serie.splice(startCol)};
                 if (_options.firstColumnHeaders) newSerie.name = serie[0];
                 actualData.push(newSerie);

                 });
                 */

                this.chart = new FRAGVIZ.D3.TemporalCurves()
                this.chart.initDisplay($(this.container)[0])
                this.chart.displayData(
                    this.chart.parseData(
                        dataSeries,
                        {
                            timeCol: 0,
                            hasHeaders: true,
                            //headers: undefined,
                            timeFormat: "%Y-%m-%d",
                            transpose: false,
                            //skipRows: '1',
                            valueCols: ['PrécipitationIn']
                        }
                        /* Worldbank data
                         {
                         timeCol: 0,
                         hasHeaders: true,
                         //headers: undefined,
                         timeFormat: "%Y",
                         transpose: true,
                         skipRows: '1',
                         //valueCols: ['Belgium', 'France', 'United States']
                         }
                         * */
                    ))
            },

            moveToContainer:function (container) {

                // call parent func
                VIEWERS.viewer.prototype.moveToContainer.apply(this, arguments);

                if (this.chart) {
                    //TODO
                }
            },

            destroy:function () {
                if (this.chart) this.chart.destroy();

                // call parent func
                VIEWERS.viewer.prototype.destroy.apply(this, arguments);
            },

            gotoFragment:function (fragment) {
                VIEWERS.viewer.prototype.gotoFragment.apply(this, arguments);
                fragment = this.currentFragment

                if (!fragment.hash) return;

                if (fragment.hash.col || fragment.hash.row || fragment.hash.cell) {

                }
            },

            //REFACTOR-DONE annotations
            displayAnnotation: function(annotation) {
                var directedPair = this.getDirectedPair(annotation);
                if (directedPair.fragment.hash.col || directedPair.fragment.hash.row || directedPair.fragment.hash.cell) {
                    //TODO
                }
            },

            refreshAnnotations: function() {
                // TODO

                if (annotationViewerClass) annotationViewerClass.prototype.refreshAnnotations.apply(this);
            }
        });


    VIEWERS.registerPrototype(
        "OpenLayers Map Viewer",
        ["application/vnd.google-earth.kml+xml", "application/gml+xml", "application/vnd.ogc.wms_xml", "application/vnd.ogc.wfs_xml", "application/vnd.geo+json"],
        {

            initViewer: function(viewerOptions) {
                this.annotFeatures = {};
                VIEWERS.viewer.prototype.initViewer.apply(this, arguments);
                this.basemaps = (viewerOptions && viewerOptions.basemaps) || VIEWERS.defaultBasemaps
            },

            acceptsURI: function(uri) {
                return this.guessMimeType(uri)
            },

            guessMimeType: function(uri) {
                var parsedUri = FRAGVIZ.UTILS.URI.parseUri(uri)
                if ( (parsedUri.queryKey.SERVICE || parsedUri.queryKey.service) == 'WMS' || parsedUri.path.indexOf('/WMSServer') != -1 || parsedUri.path.indexOf('/wms') != -1)
                    return FRAGVIZ.UTILS.MIME.getExtensionMimeType("wms")
                else if ((parsedUri.queryKey.SERVICE || parsedUri.queryKey.service) == 'WFS' || parsedUri.path.indexOf('/WFSServer') != -1 || parsedUri.path.indexOf('/wfs') != -1)
                    return FRAGVIZ.UTILS.MIME.getExtensionMimeType("wfs")
                else if ((parsedUri.queryKey.SERVICE || parsedUri.queryKey.service) == 'WMTS' || parsedUri.path.indexOf('/wmts') != -1)
                    return FRAGVIZ.UTILS.MIME.getExtensionMimeType("wmts")
                else if (parsedUri.path.endsWith('/MapServer'))
                    return FRAGVIZ.UTILS.MIME.getExtensionMimeType("arcgis_rest")

            },

            displayBase:function (uri, mimeType, callback) {
                // call parent func
                VIEWERS.viewer.prototype.displayBase.apply(this, arguments);

                var _this = this;

                var div = $("<div></div>")
                    //.attr('width', _this.container.offsetWidth+'px')
                    .css('height', '100%')
                var layerswitcherDiv = $("<div class='layerswitcher olControlLayerSwitcher'></div>")
                    .css('z-index', '1010')
                $(_this.container)
                    .empty()
                    .append(div)
                    .append(layerswitcherDiv);

                var baseMapLayer = new OpenLayers.Layer.WMSLayer(
                    'Cross Blended Hypso with Relief - Water - Drains - and Ocean Bottom',
                    'http://139.191.1.77/arcgis/services/Danube/Danube_Background/MapServer/WmsServer',
                    {layers: 'Cross Blended Hypso with Relief - Water - Drains - and Ocean Bottom',
                        transparent: true},
                    {
                        title: 'Base Layer',
                        isBaseLayer: true,
                        singleTile: false,
                        visibility: true,
                        projection: 'EPSG:4326',
                        ratio: 1,
                        //maxExtent: mapConfig['extent'] && eval(mapConfig['extent'])
                    }
                )

                // empty base layer
                var clearBaseLayer = new OpenLayers.Layer.OSM("None", OpenLayers.ImgPath + "blank.gif", {isBaseLayer: true, attribution: ''});

                var layerSwitcher = new OpenLayers.Control.HilatsLayerSwitcher(
                    {
                        'div':layerswitcherDiv[0],
                        'baselayers':_this.basemaps
                    })

                // Define callback that will be called to actually create the map
                // Resource layer will be created from uri during this process
                var createMapFn = function(baseMapLayer) {

                    var map;

                    var dataPopupControl = {

                        popup: undefined,

                        showPopup: function(feature, pixelXY) {

                            if (!this.popup || this.popup.features.indexOf(feature)<0) {

                                if (this.popup)
                                    this.hidePopup();

                                var tooltip = "<div>" + (feature.data.name || feature.fid) + "</div><table>";
                                for (var prop in feature.data) tooltip += "<tr><td>" + prop + "</td><td>" + feature.data[prop] + "</td></tr></div>"
                                tooltip += "</table>"

                                var lonlat = pixelXY && map.getLonLatFromViewPortPx({x: pixelXY.x + 5, y: pixelXY.y + 5})
                                var _thisPopupControl = this;
                                this.popup = new OpenLayers.Popup(
                                    "tooltip",
                                        lonlat || feature.geometry.getBounds().getCenterLonLat(),
                                    new OpenLayers.Size(200, 200),
                                    tooltip,
                                    //null,
                                    false,
                                    function (evt) {
                                        _thisPopupControl.hidePopup(feature);
                                    });
                                this.popup.panMapIfOutOfView = false;
                                this.popup.autoSize = true;
                                this.popup.keepInMap = true;
                                this.popup.features = []

                                this.popup.features.push(feature);
                                feature.popup = this.popup;
                                map.addPopup(this.popup);
                            }
                        },
                        hidePopup: function(feature) {
                            var popup = (feature && feature.popup) || this.popup;
                            if (popup) {
                                try {
                                    map.removePopup(popup);
                                    popup.destroy();
                                } catch (err) {
                                    // TODO sometimes destroy fails
                                }
                                if (feature && feature.popup)
                                    feature.popup = null;
                                else
                                    this.popup = null;
                            }
                        },
                        updatePopup: function(pixelXY)  {
                            var lonlat = pixelXY && map.getLonLatFromViewPortPx({x: pixelXY.x + 5, y:pixelXY.y + 5})
                            if (this.popup) {
                                this.popup.lonlat = lonlat;
                                this.popup.updatePosition();
                            }
                        }

                    }

                    var annotationPopupControl = {

                        popup: undefined,

                        showPopup: function(feature, pixelXY) {

                            if (!this.popup || this.popup.feature != feature) {

                                this.hidePopup();

                                var tooltip = "<div></div>"

                                var bounds = feature.geometry.getBounds();

                                var _thisPopupControl = this;
                                this.popup = new OpenLayers.Popup(
                                    "annotationActionBar",
                                    new OpenLayers.LonLat(bounds.right, bounds.top),
                                    null,
                                    tooltip,
                                    //null,
                                    false,
                                    function (evt) {
                                        _thisPopupControl.hidePopup(feature);
                                    });
                                this.popup.panMapIfOutOfView = true;
                                this.popup.autoSize = true;
                                this.popup.keepInMap = false;

                                this.popup.setBackgroundColor("transparent")
                                this.popup.feature = feature;

                                map.addPopup(this.popup);

                                // make sure annotation popup is on top
                                this.popup.div.style.overflow = '';
                                this.popup.div.style.zIndex = 2000;
                                this.popup.div.style.width = "0px";
                                this.popup.div.style.height = "0px";
                                this.popup.div.onmousemove = function (e) {
                                    e.stopPropagation()
                                }

                                this.popup.div.srcAnnot = feature.srcAnnot
                                //TODO add class (directedPair.isTo?"toAnnot":"fromAnnot")
                                $(this.popup.div).addClass("annot");

                                // look if there's an actions div that's been defined (either from
                                // a captured fragment, or an existing annotation)
                                feature.actionDiv && $(this.popup.div).append(feature.actionDiv)
                                feature.detailsDiv && $(this.popup.div).append(feature.detailsDiv)
                            }
                        },

                        hidePopup: function(feature) {
                            if (this.popup) {
                                map.removePopup(this.popup);
                                this.popup.destroy();
                                this.popup = null;
                            }
                        },

                        updatePopup: function(pixelXY)  {

                        }

                    }

                    var eventListeners = {
                        featureover: function (e) {
                            for (var ctrl in _this.drawControls) {
                                if (e.feature.layer == _this.drawControls[ctrl].handler.layer)
                                    return
                            }

                            e.feature.renderIntent = "select";
                            e.feature.layer.drawFeature(e.feature);

                            e.feature.onHover && e.feature.onHover(true, e)

                            var popupControl = (e.feature.layer == _this.annotationLayer) ? annotationPopupControl : dataPopupControl;

                            popupControl.showPopup(e.feature, event.xy)
                        },
                        featureout: function (e) {
                            for (var ctrl in _this.drawControls) {
                                if (e.feature.layer == _this.drawControls[ctrl].handler.layer)
                                    return
                            }

                            e.feature.renderIntent = "default"
                            e.feature.layer.drawFeature(e.feature)

                            e.feature.onHover && e.feature.onHover(false, e)

                            var popupControl = (e.feature.layer == _this.annotationLayer) ? annotationPopupControl : dataPopupControl;

                            popupControl.hidePopup(e.feature)
                        },
                        featureclick: function (e) {
                            for (var ctrl in _this.drawControls) {
                                if (e.feature.layer == _this.drawControls[ctrl].handler.layer)
                                    return
                            }

                            e.feature.onClick && e.feature.onClick(e)

                        },
                        mousemove: function (e) {
                            if (e.feature && e.feature.layer) {
                                var popupControl = (e.feature.layer == _this.annotationLayer) ? annotationPopupControl : dataPopupControl;
                                popupControl.updatePopup(event.xy)
                            }
                        }
                    }

                    var options = {
                        div: div[0],
                        layers: [baseMapLayer, clearBaseLayer],
                        maxExtent: baseMapLayer.getMaxExtent(),
                        //projection: OL_HELPERS.Mercator,
                        controls: [
                            new OpenLayers.Control.Navigation(),
                            layerSwitcher],
                        //fractionalZoom: true
                        eventListeners: eventListeners,
                        loadingDiv: $("<div class='loader' style='font-size: 10px; margin: 40px 40px; z-index: 3000'></div>")[0]
                    }
                    if (typeof OpenLayers.ThemePath !== 'undefined') options.theme = OpenLayers.ThemePath

                    // create the Map
                    map = _this.map = new OpenLayers.LoggingMap(options);

                    map.addControls([

                        //new OpenLayers.Control.Attribution()
                        //new OpenLayers.Control.PanZoomBar()
                    ]);

                    map.switchBasemap = function (baseMapLayer) {
                        if (this.baseLayer != baseMapLayer) {
                            var currentExtent = this.getExtent();
                            var currentProjection = this.getProjectionObject()
                            this.maxExtent = baseMapLayer.getMaxExtent()
                            this.setBaseLayer(baseMapLayer)
                            var newExtent = currentExtent.transform(currentProjection, baseMapLayer.projection)
                            this.zoomToExtent(newExtent)
                            //map.maxExtent = baseMapLayer.getMaxExtent()
                        }
                    }

                    var layers = _this.mapLayers = []
                    var layerAdder = function (layer) {
                        layer.isResourceLayer = true // mark layer as part of displayedResource
                        map.addLayerWithExtent(layer)
                        layers.push(layer)
                        map.addControl(layer.selector = new OpenLayers.Control.SelectFeature(layer,
                            {   clickout: true,
                                renderIntent: "select"}
                        ));

                        if (_this.annotationLayer)
                            map.setLayerIndex(_this.annotationLayer, map.getNumLayers());
                    }

                    mimeType = mimeType || _this.guessMimeType(uri)
                    var proxyUri = FRAGVIZ.UTILS.proxifyUrl(uri)

                    var deferredResult = $.Deferred()

                    if (FRAGVIZ.UTILS.MIME.getExtensionMimeType("kml") == mimeType) {
                        layerAdder(OL_HELPERS.createKMLLayer(proxyUri))
                        deferredResult.resolve()
                    } else if (FRAGVIZ.UTILS.MIME.getExtensionMimeType("gml") == mimeType) {
                        layerAdder(OL_HELPERS.createGMLLayer(proxyUri))
                        deferredResult.resolve()
                    } else if (FRAGVIZ.UTILS.MIME.getExtensionMimeType("geojson") == mimeType) {
                        layerAdder(OL_HELPERS.createGeoJSONLayer(proxyUri))
                        deferredResult.resolve()
                    } /*else if (FRAGVIZ.UTILS.MIME.getExtensionMimeType("esri_geojson") == mimeType) {
                     layerAdder(OL_HELPERS.createEsriGeoJSONLayer(proxyUri))
                     } */ else if (FRAGVIZ.UTILS.MIME.getExtensionMimeType("wms") == mimeType) {
                        OL_HELPERS.withWMSLayers(proxyUri, uri, layerAdder, undefined /*layername*/, true/*useTiling*/, map).then(function() {
                            deferredResult.resolve()
                        })
                    } else if (FRAGVIZ.UTILS.MIME.getExtensionMimeType("wmts") == mimeType) {
                        OL_HELPERS.withWMTSLayers(proxyUri, layerAdder, undefined /*layername*/, undefined/*projection*/, undefined /*resolution*/).then(function() {
                            deferredResult.resolve()
                        })
                    } else if (FRAGVIZ.UTILS.MIME.getExtensionMimeType("wfs") == mimeType) {
                        OL_HELPERS.withFeatureTypesLayers(proxyUri, layerAdder, undefined /*FTname*/, map).then(function() {
                            deferredResult.resolve()
                        })
                    } else if (FRAGVIZ.UTILS.MIME.getExtensionMimeType("arcgis_rest") == mimeType) {
                        OL_HELPERS.withArcGisLayers(proxyUri, layerAdder)
                        deferredResult.resolve()
                    }

                    // Expand layer switcher by default
                    layerSwitcher.maximizeControl();

                    deferredResult.then(function() {
                        _this.annotationLayer = new OpenLayers.Layer.Vector('Markers',
                            {displayInLayerSwitcher: false,
                                styleMap: OL_HELPERS.defaultStyleMap
                            });
                        map.addLayer(_this.annotationLayer)
                        $(_this.annotationLayer.div)
                            .toggleClass('annotationLayer', true)

                        _this.setReady(true);
                        if (callback) callback();
                    })
                }

                // Init map with first basemap from config
                OL_HELPERS.createLayerFromConfig(
                    _this.basemaps[0], // take first basemap def
                    true,
                    function(layer) {
                        _this.basemaps[0].$ol_layer = layer

                        // once basemap is instantiated, create the Map with it
                        createMapFn(layer)


                        if (_this.basemaps.length > 1) {

                            var selector = $("<select class='basemapSelector'/>")

                            $.each(_this.basemaps, function (i, mapConfig) {
                                selector
                                    .append(
                                    $('<option/>', {value: i})
                                        .text(mapConfig.title)
                                )
                            });
                            selector.change(function(e) {
                                var config = _this.basemaps[this.selectedOptions[0].value]
                                if (config.$ol_layer) {
                                    _this.map.switchBasemap(config.$ol_layer)
                                } else {
                                    OL_HELPERS.createLayerFromConfig(
                                        config,
                                        true,
                                        function(layer) {
                                            config.$ol_layer = layer
                                            _this.map.addLayer(layer)
                                            _this.map.switchBasemap(layer)
                                        });
                                }
                            });

                            $(".layersDiv").prepend(selector)

                        }

                    });


            },


            toggleCaptureMode: function(bool) {
                if (bool === undefined)
                    this.captureEnabled = ! this.captureEnabled
                else
                    this.captureEnabled = bool

                $(this.container).find(".glyphicon-screenshot").toggleClass('selected', this.captureEnabled)
                if (this.captureEnabled) {
                    this.drawControls['box'].activate()
                    this.drawControls['box'].handler.layer.removeAllFeatures()
                    this.drawControls['box'].layer.removeAllFeatures()
                    if (this.candidateAnnotation) {
                        this.annotationLayer.removeFeatures([this.candidateAnnotation])
                        this.candidateAnnotation = undefined
                    }
                } else {
                    this.drawControls['box'].deactivate()
                }
            },

            renderHoverMenu : function($menuDiv) {
                VIEWERS.viewer.prototype.renderHoverMenu.apply(this, arguments);
                var _this = this;
                $menuDiv
                    .prepend(
                    $("<i class='glyphicon-screenshot glyphicon'></i>")
                        .click(function(){
                            _this.toggleCaptureMode()
                        })
                )
            },

            allowFragmentCapture: function(callback) {
                // this will set onFragmentCapture
                VIEWERS.viewer.prototype.allowFragmentCapture.apply(this, arguments);

                var _this = this;

                var featureDrawnProcessor = function(evt) {
                    // Here, you should see the geometry of the drawn feature in your console.
                    var geom = evt.feature.geometry;
                    var selectedLayerIds = []
                    evt.object.map.layers.forEach(function(l) {
                        if (l.isResourceLayer && l.visibility)
                            selectedLayerIds.push(l.layer || l.name)
                    })

                    var wgs84Bounds = geom.getBounds().clone().transform(evt.object.map.getProjection(), OL_HELPERS.EPSG4326)


                    var fragment = {hash:{
                        id: selectedLayerIds,
                        bbox: [wgs84Bounds.toArray()]
                    }}

                    //var annotMarker = new OpenLayers.Marker(geom.getBounds().getCenterLonLat());
                    _this.candidateAnnotation = new OpenLayers.Feature.Vector(geom);
                    _this.annotationLayer.addFeatures([_this.candidateAnnotation]);

                    //TODO destroy pre-existing actionBar
                    _this.candidateAnnotation.actionDiv = $("<div></div>")

                    _this.getAnnotationRenderer().renderCapturedFragment(fragment, _this.candidateAnnotation.actionDiv, _this);

                    _this.candidateAnnotation.actionDiv
                        .css("right",'0px')
                        .css("top", '0px')
                        .css("position", 'absolute')

                    _this.toggleCaptureMode(false)
                }

                if (_this.onFragmentCapture) {
                    var pointLayer = new OpenLayers.Layer.Vector("Point Layer", {
                        'displayInLayerSwitcher':false,
                        styleMap: OL_HELPERS.defaultStyleMap
                    });
                    //var lineLayer = new OpenLayers.Layer.Vector("Line Layer");
                    //var polygonLayer = new OpenLayers.Layer.Vector("Polygon Layer");
                    var boxLayer = new OpenLayers.Layer.Vector("Box layer", {
                        'displayInLayerSwitcher':false,
                        styleMap: OL_HELPERS.defaultStyleMap
                    });

                    _this.map.addLayers([pointLayer, boxLayer]);
                    _this.map.addControl(new OpenLayers.Control.MousePosition());

                    _this.drawControls = {
                        point: new OpenLayers.Control.DrawFeature(pointLayer,
                            OpenLayers.Handler.Point),
                        /*
                         line: new OpenLayers.Control.DrawFeature(lineLayer,
                         OpenLayers.Handler.Path),
                         polygon: new OpenLayers.Control.DrawFeature(polygonLayer,
                         OpenLayers.Handler.Polygon),
                         */
                        box: new OpenLayers.Control.DrawFeature(boxLayer,
                            OpenLayers.Handler.RegularPolygon, {
                                handlerOptions: {
                                    sides: 4,
                                    irregular: true
                                }
                            }
                        )
                    };

                    for (var key in _this.drawControls) {
                        var control = _this.drawControls[key]
                        control.events.register('featureadded', control, featureDrawnProcessor);
                        _this.map.addControl(control);
                        //control.handler.layer.isCaptureLayer = true  // put a flag for generic event handlers above
                    }

                    // make sure annotation layer is on top
                    _this.annotationLayer && _this.map.setLayerIndex(_this.annotationLayer, _this.map.layers.length);
                } else {
                    //TODO remove capturing layers
                }
            },

            clearCapturedFragment: function() {
                VIEWERS.viewer.prototype.clearCapturedFragment.apply(this, arguments);

                //TODO empty selection layers
            },

            captureFragment: function(callback) {
                var sel = undefined // TODO get selected bbox
                if (sel ) {
                    callback(
                        {hash:
                        {bbox:[FRAGVIZ.CODECS.bboxCodec.encode(sel)]}
                        })
                }
            },

            moveToContainer:function (container) {

                // call parent func
                VIEWERS.viewer.prototype.moveToContainer.apply(this, arguments);

                //TODO
            },

            destroy:function () {
                // call parent func
                VIEWERS.viewer.prototype.destroy.apply(this, arguments);
            },

            gotoFragment:function (fragment) {
                VIEWERS.viewer.prototype.gotoFragment.apply(this, arguments);
                fragment = this.currentFragment

                var mapProjection   = this.map.getProjection();

                if (fragment && fragment.hash.bbox) {

                    var bbox = new OpenLayers.Bounds(fragment.hash.bbox[0]).transform(OL_HELPERS.EPSG4326, mapProjection)
                    this.map.zoomToExtent(bbox);
                    //TODO if bbox given here, do not zoom to extent of layers below
                }

                if (fragment && fragment.hash.id) {
                    var feature, bbox

                    this.mapLayers.forEach(function (layer) {
                        layer.setVisibility(false)
                    })

                    for (var i= 0, length = fragment.hash.id.length; i<length; i++) {
                        // split id in {layerName} [ / {featureId} ]
                        var idParts = fragment.hash.id[i].value.split('/')
                        var filteredLayers = this.mapLayers.filter(function (layer) {
                            return (layer.layer || layer.name) == idParts[0] //TODO correct for WFS? use getIdentifier ?
                        })

                        _.each(filteredLayers, function(layer) {
                            var layerBbox;

                            if (idParts.length>1 && layer.getFeatureById) {
                                var found = layer.getFeatureById(idParts[1])
                                if (found) {
                                    feature = found
                                    layerBbox = feature.getExtent()
                                }
                            }
                            if (idParts.length>1 && layer.getFeaturesByAttribute) {
                                var features = layer.getFeaturesByAttribute('OBJECTID', parseInt(idParts[1]))
                                if (features && features.length > 0) {
                                    feature = features[0]
                                    layerBbox = feature.getExtent()
                                }
                            }

                            if (!layerBbox) {
                                //TODO reuse logic from addLayerWithExtent
                                layerBbox = layer.getMaxExtent && layer.getMaxExtent() ||  // this works for WMTS. needed ?
                                    layer.getDataExtent && layer.getDataExtent()
                            }

                            layerBbox = layerBbox.transform(layer.projection, mapProjection)

                            layer.setVisibility(true)

                            if (bbox)
                                bbox = bbox.extend(layerBbox)
                            else
                                bbox = layerBbox
                        })
                    }

                    if (bbox) this.map.zoomToExtent(bbox);
                }
            },


            toggleAnnotationHighlight: function(annotationId, toggle) {
                if (annotationId && this.annotFeatures[annotationId]) {
                    var annotFeature = this.annotFeatures[annotationId]

                    if (toggle === undefined) toggle = !annotFeature.renderIntent == 'select';

                    annotFeature.renderIntent = toggle ? 'select' : 'default';
                    annotFeature.layer.drawFeature(annotFeature)
                }

            },

            //REFACTOR-DONE annotations
            displayAnnotation: function(annotation) {
                var directedPair = this.getDirectedPair(annotation);
                if (directedPair.fragment && directedPair.fragment.hash.bbox) {

                    var wgs84Bounds = new OpenLayers.Bounds(directedPair.fragment.hash.bbox[0]);
                    var bbox = wgs84Bounds.transform(OL_HELPERS.EPSG4326, this.map.getProjection());

                    var annotFeature = new OpenLayers.Feature.Vector(bbox.toGeometry());
                    this.annotationLayer.addFeatures([annotFeature]);

                    // register all the elements needed for annotation interactions
                    annotFeature.onHover = this.getAnnotationRenderer().createHoverListener(annotation, this)
                    annotFeature.onClick = this.getAnnotationRenderer().createClickListener(annotation, this)
                    annotFeature.detailsDiv = this.getAnnotationRenderer().renderDetailsContent(annotation, this)
                    annotFeature.actionDiv = this.getAnnotationRenderer().renderActionBarContent(annotation, this)
                    annotFeature.srcAnnot = annotation

                    this.annotFeatures[annotation] = annotFeature
                }
                if (directedPair.fragment && directedPair.fragment.hash.id) {
                    // TODO
                }
            },

            refreshAnnotations: function() {
                // TODO

                if (annotationViewerClass) annotationViewerClass.prototype.refreshAnnotations.apply(this);
            }
        });



    VIEWERS.registerPrototype(
        "CSV Viewer",
        ["application/csv", "application/x-msdownload"],
        {
            chart: undefined,
            options: {
                firstLineHeaders: true,
                firstColumnHeaders: true,
                xHeaders: undefined,
                yHeaders: undefined,
                dataColumns: {from:2}
            },

            initViewer: function(viewerOptions) {
                VIEWERS.viewer.prototype.initViewer.apply(this, arguments);


            },

            // implement 'acceptURI' to declare it as an acceptance method for viewer registration
            acceptsURI: function(uri) {
                // heuristics to match URLs such as worldbank CSV API
                return FRAGVIZ.UTILS.URI.parseUri(uri).queryKey.format == 'csv' && "application/csv"
            },

            displayBase:function (uri, mimeType, callback) {
                // call parent func
                VIEWERS.viewer.prototype.displayBase.apply(this, arguments);
                var _this= this;

                var headers = this.headers = [];
                var data = this.data = []

                //TODO make this configurable
                var readHeaders = this.readHeaders = true

                Papa.parse(FRAGVIZ.UTILS.proxifyUrl(uri), {
                    skipEmptyLines: true,
                    //worker: true,
                    beforeFirstChunk: readHeaders && function(headerLine) {
                        return headerLine.split(',').map(function(colName) { return colName.trim() }).join(',')
                    },
                    dynamicTyping: true,
                    header: readHeaders,
                    download: true,
                    step: function(row) {
                        if (_this.headers.length == 0) {
                            if (readHeaders)
                                _this.setHeaders(row.meta.fields)
                            else {
                                // generate headers for every column
                                _this.setHeaders(row.data[0].map(function(val, idx) {
                                    return "COL"+idx
                                }))
                                _this.appendRow(row.data[0])
                            }

                        }
                        else
                        //TODO check if row.data has one single element
                            _this.appendRow(row.data[0])
                    },
                    complete: function(result) {
                        _this.setReady(true);
                        if (callback) callback()
                    }
                });

                /*
                 $.ajax(FRAGVIZ.UTILS.proxifyUrl(uri), {
                 // TODO doesn't work : OpenBelgium demo : FR headers to force metric system on wunderground weather
                 headers: {'Accept-Language' : 'fr-FR,fr;q=0.8,en;q=0.4'}
                 })
                 .done(function(a) {
                 a = a.trim() // trim the result (remove occurring leading or trailing CRs, ...)
                 var data = d3.csv.parseRows(a)
                 _this.displayData(data);
                 _this.setReady(true);
                 if (callback) callback()
                 })
                 */
            },

            setHeaders: function(headers) {
                this.headers = headers;
                //this.displayTable()
                this.displayGraph()
            },

            appendRow: function(datarow) {
                if (!this.readHeaders) {
                    var newDatarow = {}
                    for (var idx = 0, length = datarow.length; idx < length; idx++)
                        newDatarow[this.headers[idx]] = datarow[idx]
                    datarow = newDatarow
                }

                this.data.push(datarow)
                if (this.table) {
                    this.table.row.add(datarow).draw()
                }
                if (this.view) {
                    this.view.insert('table', [datarow]).run()
                }
            },

            displayTable: function() {
                if (this.table)
                    this.table.destroy();

                var $table = $(this.container).find("table.datatable")
                if (!$table.length)
                    $(this.container).append($table = $("<table class='datatable display'></table>"))

                var originalWidth = $(this.container).width()

                this.table = $table.DataTable({
                    //data: this.data,
                    scrollX: true,
                    columns: this.headers.map(function(name) {
                        return {title: name, data: name}
                    })
                });

                // hardwire width to enforce scrolling in table
                $(this.container).find(".dataTables_wrapper").css('width', originalWidth)
            },

            displayGraph: function() {

                if (this.view)
                    this.view.finalize();

                var $chartContainer = $(this.container).find("div.chart-container")
                if (!$chartContainer.length)
                    $(this.container).append($chartContainer = $("<div class='chart-container'></div>"))

                console.log("graph w/h: "+$(this.container).width()+","+$(this.container).height())
                var vegaSpec = {
                    "$schema": "https://vega.github.io/schema/vega/v3.0.json",
                    "width": $(this.container).width(),
                    "height": $(this.container).height(),
                    "padding": 15,
                    "autosize" : "fit",


                    "signals": [
                        {
                            "name": "interpolate",
                            "value": "linear",
                            /*
                             "bind": {
                             "input": "select",
                             "options": [
                             "basis",
                             "cardinal",
                             "catmull-rom",
                             "linear",
                             "monotone",
                             "natural",
                             "step",
                             "step-after",
                             "step-before"
                             ]
                             }
                             */
                        },

                        {
                            "name": "tooltip",
                            "value": {},
                            "on": [
                                {"events": "symbol:mouseover", "update": "datum"},
                                {"events": "symbol:mouseout",  "update": "{}"}
                            ]
                        },

                        {
                            "name": "hoveredX",
                            "value": {},
                            "on": [
                                {
                                    "events": "mousemove",
                                    "update": "x()"
                                }
                            ]
                        },
                    ],

                    "data": [
                        {
                            "name": "table",

                             "values": [
                             {"CEST": "2014-5-1", "Température maximumC": 28, "Température moyenneC":17},
                             {"CEST": "2014-5-2", "Température maximumC": 23, "Température moyenneC":18},
                             {"CEST": "2014-5-3", "Température maximumC": 24, "Température moyenneC":13}
                             ]

                        },

                        {
                            "name": "hovered",
                            "source": "table",
                            "transform": [
                                {
                                    "type": "filter",
                                    "expr": "scale('x', datum['"+this.headers[0]+"']) > hoveredX-5 && scale('x', datum['"+this.headers[0]+"']) < hoveredX+5"
                                }
                            ]
                        },
                    ],

                    "scales": [
                        {
                            "name": "x",
                            "type": "point",
                            "range": "width",
                            "domain": {"data": "table", "field": this.headers[0]}
                        },
                        {
                            "name": "y",
                            "type": "linear",
                            "range": "height",
                            "nice": true,
                            "zero": true,
                            "domain": {"data": "table", "field": this.headers[1]}
                        }
                    ],

                    "axes": [
                        {"orient": "bottom", "scale": "x"},
                        {"orient": "left", "scale": "y"}
                    ],


                    "marks": [
                        {
                            "name": "lines",
                            "type": "line",
                            "from": {"data": "table"},
                            "encode": {
                                "update": {
                                    "x": {"scale": "x", "field": this.headers[0]},
                                    "y": {"scale": "y", "field": this.headers[1]},
                                    "interpolate": {"signal": "interpolate"},
                                    //"stroke": {"scale": "color", "field": "c"},
                                    "strokeWidth": {"value": 2}
                                },
                                /*
                                 "update": {
                                 "interpolate": {"signal": "interpolate"},
                                 "fillOpacity": {"value": 1}
                                 },
                                 */
                                "hover": {
                                    //"opacity": {"value": 0.5}
                                }
                            }
                        },

                        {
                            "type": "text",
                            "from": {"data": "hovered"},
                            "encode": {
                                "update": {
                                    "align": {"value": "center"},
                                    "baseline": {"value": "bottom"},
                                    "fontSize": {"value": 13},
                                    "fontWeight": {"value": "bold"},
                                    "text": {"field": this.headers[1]},
                                    "x": {"scale": "x", "field": this.headers[0]},
                                    "y": {"scale": "y", "field": this.headers[1]}
                                }
                            }
                        },

                        /*
                        {
                            "type": "text",
                            "encode": {
                                "update": {
                                    "align": {"value": "center"},
                                    "baseline": {"value": "top"},
                                    "fontSize": {"value": 13},
                                    //"fontWeight": {"value": "bold"},
                                    "text": {"signal": "tooltip['"+this.headers[1]+"']"},
                                    "x": {"scale": "x", "signal": "tooltip['"+this.headers[0]+"']"},
                                    "y": {"scale": "y", "signal": "tooltip['"+this.headers[1]+"']"},
                                    "fillOpacity": [
                                        {"test": "datum === tooltip", "value": 0},
                                        {"value": 1}
                                    ]
                                }
                            }
                        },
                        */


                        {
                            "type": "symbol",
                            "from": {"data": "table"},
                            "encode": {
                                 "update": {
                                     "x": {"scale": "x", "field": this.headers[0]},
                                     "y": {"scale": "y", "field": this.headers[1]},
                                     "stroke": {"value": "steelblue"},
                                     "strokeWidth": {"value": 1.5},
                                     "fill": {"value": "white"},
                                     "size": {"value": 30}
                                 },
                                "hover": {
                                    "fill": {"value": "lightgreen"},
                                    "size": {"value": 60}
                                }
                            }
                        }

                    ]
                }


                this.view = new vega.View(vega.parse(vegaSpec), {
                    //loader: vega.loader({baseURL: 'https://vega.github.io/vega/'}),
                    logLevel: vega.Warn,
                    renderer: 'svg'
                }).initialize($chartContainer[0]).hover().run();

            },

            moveToContainer:function (container) {

                // call parent func
                VIEWERS.viewer.prototype.moveToContainer.apply(this, arguments);

                if (this.chart) {
                    //TODO
                }
            },

            destroy:function () {
                if (this.chart) this.chart.destroy();

                // call parent func
                VIEWERS.viewer.prototype.destroy.apply(this, arguments);
            },

            gotoFragment:function (fragment) {
                VIEWERS.viewer.prototype.gotoFragment.apply(this, arguments);
                fragment = this.currentFragment

                if (!fragment.hash) return;

                if (fragment.hash.col || fragment.hash.row || fragment.hash.cell) {

                }
            },

            //REFACTOR-DONE annotations
            displayAnnotation: function(annotation) {
                var directedPair = this.getDirectedPair(annotation);
                if (directedPair.fragment && (directedPair.fragment.hash.col || directedPair.fragment.hash.row || directedPair.fragment.hash.cell)) {
                    //TODO
                }
            },

            refreshAnnotations: function() {
                // TODO

                if (annotationViewerClass) annotationViewerClass.prototype.refreshAnnotations.apply(this);
            }
        });


    /**
     * Util method to parse Youtube URIs
     */
    VIEWERS.SLIDESHARE = {
        extractSlideshareId: function(uri) {
            var parsedURI = FRAGVIZ.UTILS.URI.parseUri(uri);
            if (parsedURI.protocol == 'slideshare') return parsedURI.file;
            // parse this: http://static.slidesharecdn.com/swf/ssplayer2.swf?doc=osconopendatabizmodels-110729224250-phpapp01
            else if (parsedURI.host.indexOf('static.slidesharecdn.com') >= 0 && parsedURI.file == "ssplayer2.swf") return parsedURI.query.doc[0];
            else return undefined;
        }
    };

    VIEWERS.registerPrototype(
        "Slideshare Viewer",
        ["application/slideshare"],
        {
            slidesPlayer: undefined,

            // implement 'acceptURI' to declare it as an acceptance method for viewer registration
            acceptsURI: function(uri) {
                return VIEWERS.SLIDESHARE.extractSlideshareId(uri) && "application/slideshare";
            },

            displayBase: function (uri, mimeType, callback) {

                // call parent func
                VIEWERS.viewer.prototype.displayBase.apply(this, arguments);

                var slideshareId = VIEWERS.SLIDESHARE.extractSlideshareId(uri);

                var slidesUrl = "http://static.slidesharecdn.com/swf/ssplayer2.swf?doc=osconopendatabizmodels-110729224250-phpapp01";
                this.slidesPlayer = $('<object width="100%" height="100%">' +
                    '<param name="movie" value="'+slidesUrl+'" />' +
                    '<param name="allowScriptAccess" value="always"/>' +
                    '<param name="wmode" value="transparent"/>' +
                    '<embed src="'+slidesUrl+'" type="application/x-shockwave-flash" allowscriptaccess="always" wmode="transparent"></embed>' +
                    '</object>')[0];

                $(this.container).empty()
                    .append(this.slidesPlayer);

            },

            moveToContainer:function (container) {

                // call parent func
                VIEWERS.viewer.prototype.moveToContainer.apply(this, arguments);

                if (this.slidesPlayer) {
                    $(container).empty()
                        .append(this.slidesPlayer);
                }
            },

            destroy: function() {
                if (this.slidesPlayer) $(this.slidesPlayer).remove();

                // call parent func
                VIEWERS.viewer.prototype.destroy.apply(this, arguments);
            },

            gotoFragment: function(fragment) {
                VIEWERS.viewer.prototype.gotoFragment.apply(this, arguments)
                fragment = this.currentFragment

                if (fragment.hash && fragment.hash.page) {
                    // TODO do smthg with fragment.hash.page[0].start
                }
            },

            displayAnnotation: function(annotation) {
                //TODO
            }

        });


    /**
     * JPlayer viewer
     * (Currently deactivated)
     */
    VIEWERS.registerPrototype(
        "JPlayer Viewer",
        // JPlayer deactivated - no mimetype registered
        [], //["video/ogg", "video/mp4", "video/webm", "video/youtube"],
        {   displayBase:
            function (uri, mimeType) {

                var typeMap = {
                    "video/mp4" : "m4v",
                    "video/ogg" : "ogv",
                    "video/webm" : "webmv",
                    "audio/mp3" : "mp3",
                    "audio/mp4" : "m4a",
                    "audio/ogg" : "oga"
                };


                var playerDiv = $(this.container).empty().html(
                        '<div id="jquery_jplayer" class="jp-jplayer" style="width:100%;height:100%"></div>' +
                        '<div id="jp_interface_1" class="jp-interface">' +
                        '<div class="jp-video-play"></div>' +
                        '<ul class="jp-controls">' +
                        '<li><a href="#" class="jp-play">Play</a></li>' +
                        '<li><a href="#" class="jp-pause">Pause</a></li>' +
                        '</ul>' +
                        '<div class="jp-progress">' +
                        '<div class="jp-seek-bar">' +
                        '<div class="jp-play-bar"></div>' +
                        '</div></div>' +
                        '<div class="jp-volume-bar">' +
                        '<div class="jp-volume-bar-value"></div>' +
                        '</div>' +
                        ' <div class="jp-current-time"></div>' +
                        ' <div class="jp-duration"></div>' +
                        '</div>');

                var type = typeMap[mimeType];

                $('#jquery_jplayer').jPlayer({
                    ready: function () {
                        var mediaConf = {};
                        mediaConf[type] = uri;
                        $(this).jPlayer("setMedia", mediaConf);
                    },
                    swfPath: "../ecmascript/jplayer",
                    supplied: type
                });


            }
        });

    var MIMEWRAPPERS = root.FRAGVIZ.MIMEWRAPPERS = {}
    MIMEWRAPPERS.AbstractWrapper = function(frame) {
        if (annotationViewerClass) annotationViewerClass.apply(this)
        this.frame = frame;
        this.init();
    }
    _.extend(MIMEWRAPPERS.AbstractWrapper.prototype, annotationViewerClass ? annotationViewerClass.prototype : {},
        {
            init: function() {
                this.frame.mimeWrapper = this;
                this.currentSelection = undefined;
                this.selectionListeners = [];
            },

            gotoFragment: function(fragment) {
                throw "Not implemented in abstract viewer"
            },

            getParsedFragment: function(fragment) {
                if (typeof fragment == "object" && fragment.hash) return fragment;
                else if (typeof fragment == "string") return FRAGVIZ.UTILS.parseFragment(fragment)
                else return undefined
            },

            triggerSelectionChange: function() {
                var _this = this;
                _.each(this.selectionListeners, function(listener) {listener(_this.currentSelection)})
            },

            onSelectionChange: function(listener) {
                this.selectionListeners.push(listener);
            }

        })


    MIMEWRAPPERS.HtmlWrapper = function(frame) {
        MIMEWRAPPERS.AbstractWrapper.apply(this, [frame]);
    }
    _.extend(
        MIMEWRAPPERS.HtmlWrapper.prototype,
        MIMEWRAPPERS.AbstractWrapper.prototype,
        {
            init: function() {
                MIMEWRAPPERS.AbstractWrapper.prototype.init.apply(this, arguments)
                this.annotBoxes = []
                this.initSelectionListener();
            },

            initSelectionListener: function() {
                var _this = this;
                this.onTextSelect = function(e) {
                    var sel = FRAGVIZ.UTILS.getSelectionRange(window);
                    if (sel && (sel.startOffset != sel.endOffset || sel.startContainer != sel.endContainer)) {
                        (_this.currentSelection || (_this.currentSelection = {})).xpath = [FRAGVIZ.CODECS.xpathCodec.getXPathFragmentFromRange(sel)]
                        _this.triggerSelectionChange();
                    }
                };
                $(this.frame).mouseup(this.onTextSelect);
            },

            gotoFragment: function(fragment) {
                fragment = this.getParsedFragment(fragment);

                if (!fragment.hash) return;

                if (fragment.hash.xpath) {
                    var selRange = FRAGVIZ.UTILS.getRangeFromXPathFragment(
                        fragment.hash.xpath[0].startXpath,
                        fragment.hash.xpath[0].startOffset,
                        fragment.hash.xpath[0].endXpath,
                        fragment.hash.xpath[0].endOffset,
                        this.frame.document
                    );

                    FRAGVIZ.UTILS.setSelectionRange(this.frame, selRange);

                    this.frame.scrollTo(0, FRAGVIZ.UTILS.computeTotalOffset(selRange.startContainer)[1] - 30);
                    //if (selRange.startContainer.scrollIntoView) selRange.startContainer.scrollIntoView();
                    //else if (selRange.startContainer.parentNode) selRange.startContainer.parentNode.scrollIntoView();
                }
            },

            gotoAnnotation: function(annotation, followTo) {
                if (typeof annotation == "string") annotation = this.annotationList.getById(annotation);

                if (annotation) {
                    var fromToFrags = annotation.getDirections(this.frame.location)
                    this.gotoFragment((!followTo && fromToFrags.from) || fromToFrags.to);
                }
            },

            toggleAnnotationHighlight: function(annotation, toggle) {

                if (typeof annotation == "string") annotation = this.annotationList.getById(annotation);

                if (annotation) {
                    if (toggle === undefined) toggle = !$(this.annotBoxes[annotation.getId()]).hasClass("active");

                    if (toggle) $(this.annotBoxes[annotation.getId()]).addClass("active")
                    else $(this.annotBoxes[annotation.getId()]).removeClass("active")
                }
            },

            clearAnnotationDisplay: function() {
                for (var annot in this.annotBoxes) {
                    $(this.annotBoxes[annot]).remove();
                }
            },

            //REFACTOR-DONE annotations
            displayAnnotation: function(annotation) {
                if (!this.frame.document) FRAGVIZ.LOG.warn("Trying to display annotations before document is loaded");

                var fragment;

                // check whether this viewer matches the annotation src or target
                var fromToFrags = annotation.getDirections(this.frame.location)
                if (!fromToFrags) return

                //TODO if both 'from' and 'to' are present, it defaults to 'from'. Correct ?
                var fragment = fromToFrags.from || fromToFrags.to

                if (fragment.hash && fragment.hash.xpath) {
                    var selRange = FRAGVIZ.UTILS.getRangeFromXPathFragment(
                        fragment.hash.xpath[0].startXpath,
                        fragment.hash.xpath[0].startOffset,
                        fragment.hash.xpath[0].endXpath,
                        fragment.hash.xpath[0].endOffset,
                        this.frame.document
                    );

                    var bbox = FRAGVIZ.UTILS.computePreciseRangeXywh(selRange);

                    var annotBox = $("<div><div></div></div>");

                    // make and append div containg the details of the annotation
                    var detailsDiv = $("<div></div>").addClass("details")
                    detailsDiv.html("<b>"+annotation.name+"</b><br><span>"+annotation.description+"</span>")
                    annotBox.append(detailsDiv)

                    // make and append link icon
                    var linkIcon = $("<div></div>").addClass("linkIcon32")
                    annotBox.append(linkIcon)

                    annotBox[0].srcAnnot = annotation;

                    // add annotation DIV to list of displayed annotations
                    this.annotBoxes[annotation.getId()] = annotBox[0];

                    $('body', this.frame.document).append(annotBox);
                    // class must be added after appending to be taken into account
                    var padding = 2;

                    annotBox.css("top", (bbox[1]-padding)+'px')
                        .css("left", (bbox[0]-padding)+'px')
                        .css("width", (bbox[2]-bbox[0]+(2*padding))+'px')
                        .css("height", (bbox[3]-bbox[1]+(2*padding))+'px')
                        .addClass("annot "+ (fromToFrags.from?"fromAnnot":"toAnnot"));
                }
            },

            refreshAnnotations: function() {
                if (annotationViewerClass) annotationViewerClass.prototype.refreshAnnotations.apply(this);

                $(this.frame)
                    .scrolllinks({
                        anchorSelector: '.annot',
                        content: function (i, $page) {
                            return '<span class="scrolllink">' +( $page[0].srcAnnot.description || $page[0].srcAnnot.name)+ '</span>';
                        }
                    });
                //TODO append a full-size background transparent overlay with a hover-on effect
            }
        });

}) .call(this);

/**
 * Returns scale factor for the canvas. It makes sense for the HiDPI displays.
 * @return {Object} The object with horizontal (sx) and vertical (sy)
 scales. The scaled property is set to false if scaling is
 not required, true otherwise.
 */
function getOutputScale(ctx) {
    var devicePixelRatio = window.devicePixelRatio || 1;
    var backingStoreRatio = ctx.webkitBackingStorePixelRatio ||
        ctx.mozBackingStorePixelRatio ||
        ctx.msBackingStorePixelRatio ||
        ctx.oBackingStorePixelRatio ||
        ctx.backingStorePixelRatio || 1;
    var pixelRatio = devicePixelRatio / backingStoreRatio;
    return {
        sx: pixelRatio,
        sy: pixelRatio,
        scaled: pixelRatio !== 1
    };
}